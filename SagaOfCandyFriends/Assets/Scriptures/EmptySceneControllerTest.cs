﻿using UnityEngine;
using System.Collections;

public class EmptySceneControllerTest : MonoBehaviour 
{
	public int playerNum;
	public float moveSpeed;

	Vector3 myVelo;
	float xIn, yIn;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		myVelo = rigidbody.velocity;

		xIn = Input.GetAxis("P" + playerNum.ToString() + "_Horizontal") * moveSpeed;
		yIn = Input.GetAxis("P" + playerNum.ToString() + "_Vertical") * moveSpeed;

		myVelo.x = xIn;
		myVelo.y = yIn;

		rigidbody.velocity = myVelo;

		if(Input.GetButtonDown("P" + playerNum.ToString() + "_Button1"))
		{
			iTween.ColorTo(gameObject, iTween.Hash("r", 0.0f, "g", 1.0f, "b", 0.0f, "time", 0.25f, "easetype", iTween.EaseType.easeInQuad));
		}
		if(Input.GetButtonUp ("P" + playerNum.ToString () + "_Button1"))
		{
			iTween.ColorTo(gameObject, iTween.Hash("r", 1.0f, "g", 1.0f, "b", 1.0f, "time", 0.25f, "delay", 0.0f, "easetype", iTween.EaseType.easeOutQuad));
		}

		if(Input.GetButtonDown("P" + playerNum.ToString() + "_Button2"))
		{
			iTween.ColorTo(gameObject, iTween.Hash("r", 0.0f, "g", 0.0f, "b", 1.0f, "time", 0.25f, "easetype", iTween.EaseType.easeInQuad));
		}
		if(Input.GetButtonUp("P" + playerNum.ToString() + "_Button2"))
		{
			iTween.ColorTo(gameObject, iTween.Hash("r", 1.0f, "g", 1.0f, "b", 1.0f, "time", 0.25f, "delay", 0.0f, "easetype", iTween.EaseType.easeOutQuad));
		}
	}
}
