﻿using UnityEngine;
using System.Collections;

public class RyanVineVine : MonoBehaviour 
{
	HingeJoint myJoint;
	float myLength;
	public RyanVinePlayer myPlayer = null;

	// Use this for initialization
	void Start () 
	{
		myJoint = transform.parent.hingeJoint;
		myLength = transform.localScale.y * transform.parent.localScale.y;
	}	

	public void AttachPlayerToSelf(Rigidbody anchorObject, RyanVinePlayer player)
	{
		myPlayer = player;
		myJoint.connectedBody = anchorObject;
		player.GetVineJointPosition(myJoint.transform.position, myLength);

		renderer.enabled = false;
	}

	public void DetachPlayer()
	{
		myJoint.connectedBody = null;
		renderer.enabled = true;
		myPlayer = null;
	}
}
