﻿using UnityEngine;
using System.Collections;

public class RyanVineCandySpinnaz : MonoBehaviour 
{
	GameObject candyToSpin;

	bool ableToBeCollected = true;

	bool hasTarget = false;
	Transform myTarget;

	bool isRotating = false;
	Vector3 myEulers = Vector3.zero;
	Transform rotObj;
	float lerpPercent = 0.15f;

	// Use this for initialization
	void Start () 
	{
		candyToSpin = transform.Find("RyanVineCandyCyl").gameObject;
		rotObj = transform.Find("rotObj");

		//iTween.RotateTo(candyToSpin, iTween.Hash("y", 180.0f, "time", 2.5f, "easetype", iTween.EaseType.easeInOutQuad, "looptype", iTween.LoopType.loop));
		iTween.RotateTo(candyToSpin, iTween.Hash("z", 80.0f, "time", 2.5f, "easetype", iTween.EaseType.easeInOutQuad, "oncomplete", "TriggerTweenPingPong", "oncompletetarget", gameObject));
	}

	void TriggerTweenPingPong()
	{
		iTween.RotateTo(candyToSpin, iTween.Hash("z", -80.0f, "time", 2.5f, "easetype", iTween.EaseType.easeInOutQuad, "looptype", iTween.LoopType.pingPong));
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(hasTarget)
		{
			transform.position = Vector3.Lerp(transform.position, myTarget.position, lerpPercent);
		}

		if(isRotating)
		{
			myEulers.y = rotObj.position.y;
			transform.eulerAngles = myEulers;
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(ableToBeCollected)
		{
			//hasTarget = true;
			ableToBeCollected = false;
			collider.enabled = false;

			RyanVinePlayer player = other.GetComponent<RyanVinePlayer>();
			Debug.Log(other.name);
			//player.AddCandy();

			iTween.Stop(candyToSpin);

			iTween.ScaleTo(gameObject, iTween.Hash("x", 1.25f, "y", 1.25f, "z", 1.25f, "time", 1.0f, "easetype", iTween.EaseType.easeOutBounce, "oncomplete", "TrackPlayer", "oncompletetarget", gameObject, "oncompleteparams", player));

			isRotating = true;
			rotObj.parent = null;
			//rotObj.position = Vector3.zero;
			iTween.MoveTo(rotObj.gameObject, iTween.Hash("y", 1080.0f, "time", 1.5f, "easetype", iTween.EaseType.easeInOutQuad));

			//iTween.RotateTo(gameObject, iTween.Hash("y", 360.0f, "time", 0.5f, "delay", 0.5f, "easetype", iTween.EaseType.easeOutQuad));
		}
		//Destroy(gameObject);
	}

	void TrackPlayer(RyanVinePlayer playerToTrack)
	{
		iTween.ScaleTo(gameObject, iTween.Hash("x", 0.5f, "y", 0.5f, "z", 0.5f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutQuad));

		hasTarget = true;
		myTarget = playerToTrack.candyHoverPos;
		playerToTrack.AddCandy();

		ParticleEmitter[] parts = GetComponentsInChildren<ParticleEmitter>() as ParticleEmitter[];
		foreach(ParticleEmitter p in parts)
			p.emit = false;

		lerpPercent -= (playerToTrack.numCandies * 0.01f);
		//if(playerToTrack.numCandies <= 10)


		Debug.Log("Player has " + playerToTrack.numCandies + " candies, lerping at " + lerpPercent.ToString());
	}
}
