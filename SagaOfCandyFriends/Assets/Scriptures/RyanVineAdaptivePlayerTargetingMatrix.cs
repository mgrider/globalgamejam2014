﻿using UnityEngine;
using System.Collections;

public class RyanVineAdaptivePlayerTargetingMatrix : MonoBehaviour 
{
	public float timeUntilDamageDealt;
	float[] damageTimers;
	RyanVinePlayer[] targetedPlayers;
	bool[] isCountingDown;
	bool[] isBeingTargeted;

	int myCandies;
	RyanVinePlayer myOwner;

	bool hasAnyTarget = false;

	public GameObject attackParticle;

	// Use this for initialization
	void Start () 
	{
		//damageTimers = new List<float>();
		//targetedPlayers = new List<RyanVinePlayer>();
		//isCountingDown = new List<bool>();
		damageTimers = new float[4];
		targetedPlayers = new RyanVinePlayer[4];
		isCountingDown = new bool[4];
		isBeingTargeted = new bool[4];

		for(int i=0;i<isBeingTargeted.Length;i++)
		{
			isBeingTargeted[i] = false;
			isCountingDown[i] = false;
			damageTimers[i] = timeUntilDamageDealt;
		}

		myOwner = transform.parent.GetComponent<RyanVinePlayer>();
		Debug.Log (myOwner.name);
	}
	
	// Update is called once per frame
	void Update () 
	{
		hasAnyTarget = false;

		for(int i=0;i<isBeingTargeted.Length;i++)
		{
			if(isBeingTargeted[i])
			{
				if(isCountingDown[i])
				{
					damageTimers[i] -= Time.deltaTime;
					if(damageTimers[i] <= 0)
					{
						Kamehameha();
					}
				}
				else
				{
					damageTimers[i] += (Time.deltaTime / 2);
					if(damageTimers[i] >= timeUntilDamageDealt)
					{
						isCountingDown[i] = false;
						isBeingTargeted[i] = false;
						targetedPlayers[i] = null;
						damageTimers[i] = timeUntilDamageDealt;
					}
				}

				hasAnyTarget = true;
			}
		}

		//Debug.Log("P1 Health: " + damageTimers[0] + " P2 Health: " + damageTimers[1]);
	}

	void OnTriggerEnter(Collider other)
	{
		//Debug.Log(other.name);
		if(other.tag != "VineAnchor")
		{
			RyanVinePlayer otherPlayer = other.GetComponent<RyanVinePlayer>();

			if(otherPlayer.numCandies != myCandies)
			{
				int otherIndex = otherPlayer.playerArrayIndex;

				//if(otherPlayer.numCandies
				isBeingTargeted[otherIndex] = true;
				isCountingDown[otherIndex] = true;
				targetedPlayers[otherIndex] = otherPlayer;
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.tag != "VineAnchor")
		{
			RyanVinePlayer otherPlayer = other.GetComponent<RyanVinePlayer>();
			int otherIndex = otherPlayer.playerArrayIndex;
			
			//isBeingTargeted[otherIndex] = true;
			isCountingDown[otherIndex] = false;
		}
	}

	void CheckVisibilityOfTargets()
	{
		//todo: All of this nonsense
	}

	void Kamehameha()
	{
		Debug.Log("KameHameHAAAA");
		//todo: deal damage
		for(int i=0;i<isBeingTargeted.Length;i++)
		{
			if(isCountingDown[i]) //only hit players we can currently see
			{
				targetedPlayers[i].SetStatus(myOwner);
			}

			isBeingTargeted[i] = false;
			isCountingDown[i] = false;
			targetedPlayers[i] = null;
		}

		GameObject particles = (GameObject)Instantiate(attackParticle, myOwner.transform.position, myOwner.transform.rotation);
		particles.transform.parent = myOwner.transform;
		particles.transform.LookAt(transform);

		Destroy(particles, 1.1f);
	}

	public void UpdateSelfCandies(int newCandies)
	{
		myCandies = newCandies;
	}
}
