﻿using UnityEngine;
using System.Collections;

public class RyansAwesomeSpritesheetAnimatorVersionTwoPointOh : MonoBehaviour 
{
	public int numFrames;
	int currentState = 0;
	public string[] stateNames;

	int currentFrameIndex = 0;

	public Vector2[] idleFrames;
	public Vector2[] runningFrames;

	public float[] frameUpdateIntervals;
	float frameTimer;

	// Use this for initialization
	void Start () 
	{
		frameTimer = frameUpdateIntervals[0];
	}
	
	// Update is called once per frame
	void Update () 
	{
		frameTimer -= Time.deltaTime;
		if(frameTimer <= 0)
		{
			ChangeFrame();
		}
	}

	void ChangeFrame()
	{
		currentFrameIndex++;

		if(currentState == 0)
		{
			if(currentFrameIndex > idleFrames.Length-1)
				currentFrameIndex = 0;

			renderer.material.mainTextureOffset = idleFrames[currentFrameIndex];
		}
		else
		{
			if(currentFrameIndex > runningFrames.Length-1)
				currentFrameIndex = 0;

			renderer.material.mainTextureOffset = runningFrames[currentFrameIndex];
		}
		
		frameTimer = frameUpdateIntervals[currentState];
	}

	public void ChangeState(string newStateName)
	{
		if(newStateName.ToLower() == "idle")
			ChangeState(0);
		else if(newStateName.ToLower() == "running")
			ChangeState(1);
	}

	public void ChangeState(int newStateIndex)
	{
		currentState = newStateIndex;
		currentFrameIndex = 0;
		ChangeFrame();
	}

	public void FaceLeft()
	{
		renderer.material.mainTextureScale = new Vector2(-1, 0.125f);
	}

	public void FaceRight()
	{
		renderer.material.mainTextureScale = new Vector2(1, 0.125f);
	}

	public void SetAnimSpeed(int stateToChange, float newSpeed)
	{
		frameUpdateIntervals[stateToChange] = newSpeed;
	}
}
