﻿using UnityEngine;
using System.Collections;

public class VineTimer : MonoBehaviour 
{
	public int numMinutes;
	public float numSeconds;

	GUIText timerInScene;
	public RyanVinePlayer[] players;

	bool isGameOver = false;

	// Use this for initialization
	void Start () 
	{
		timerInScene = guiText;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!isGameOver)
		{
			numSeconds -= Time.deltaTime;
			if(numSeconds <= 0)
			{
				if(numMinutes > 0)
				{
					numMinutes--;
					numSeconds = 60;
				}
				else
					GameOver();
			}

			UpdateSceneTimer();
		}

		if ( Input.GetKeyDown(KeyCode.Escape) )
		{
			Application.LoadLevel("MainMenu");
		}
	}

	void GameOver()
	{
		isGameOver = true;

		foreach(RyanVinePlayer p in players)
			p.FuckingDieAlready();

		numSeconds = 0;
	}

	void UpdateSceneTimer()
	{
		if(isGameOver)
		{
			timerInScene.text = "0:00";
		}
		else
		{
			if(numSeconds < 10)
				timerInScene.text = numMinutes.ToString() + ":0" + Mathf.Floor(numSeconds);
			else
				timerInScene.text = numMinutes.ToString() + ":" + Mathf.Floor(numSeconds);
		}
	}
}
