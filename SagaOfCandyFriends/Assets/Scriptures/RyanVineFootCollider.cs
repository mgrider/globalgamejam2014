﻿using UnityEngine;
using System.Collections;

public class RyanVineFootCollider : MonoBehaviour 
{
	RyanVinePlayer myOwner;

	void Start()
	{
		myOwner = transform.parent.GetComponent<RyanVinePlayer>();
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag != "VineCandy")
			myOwner.MakeGrounded();
	}

	void OnTriggerExit(Collider other)
	{
		if(other.tag != "VineCandy")
			myOwner.MakeUngrounded();
	}
}
