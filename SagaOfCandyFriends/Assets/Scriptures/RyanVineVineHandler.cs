﻿using UnityEngine;
using System.Collections;

public class RyanVineVineHandler : MonoBehaviour 
{
	bool hasVine = false;
	GameObject myVine;

	RyanVinePlayer myOwner;

	void Start()
	{
		myOwner = transform.parent.GetComponent<RyanVinePlayer>();
	}

	void Update()
	{
		//Debug.Log(hasVine + Time.timeSinceLevelLoad.ToString());
	}

	void OnTriggerEnter(Collider other)
	{
		if(!hasVine)
		{
			myVine = other.gameObject;
			hasVine = true;

			//Attempt to allow vine stealing.  Not working yet.  Maybe DetachVine isn't enough, or it's constantly doing detach/reattach?
			RyanVineVine vineObj = myVine.GetComponent<RyanVineVine>();
			RyanVinePlayer currentSwinger = vineObj.myPlayer;
			if(currentSwinger != null)
				currentSwinger.DetachVine();
			myOwner.GetVineNearby(vineObj);
			myOwner.AttachVine();
		}
	}

//	void OnTriggerExit(Collider other)
//	{
//		if(hasVine)
//		{
//			if(other.gameObject == myVine)
//			{
//				if(!myOwner.isVined)
//				{
//					myVine = null;
//					hasVine = false;
//
//					myOwner.LoseVineNearby();
//				}
//			}
//		}
//	}

	public void LoseVineInfo()
	{
		myVine = null;
		hasVine = false;
	}
}
