﻿using UnityEngine;
using System.Collections;

public class RyanVinePlayer : MonoBehaviour 
{
	public int playerNum;
	public float moveSpeed;
	public float airControl = 1.25f;

	Vector3 myVelo;
	float xIn, yIn;

	string horizInputString, vertInputString, buttonString1, buttonString2;

	public bool isVined = false;
	Rigidbody vineAnchor;

	public float maxSpeed;

	bool isGrounded;

	public float jumpSpeed;

	bool hasVineNearby = false;
	RyanVineVine vineNearby;

	LineRenderer myLine;
	Vector3 vineJointPos;

	RyanVineVineHandler myVineHandler;
	float remainingVineLength;

	//Transform aimTransform;
	//Vector3 aimPos = Vector3.zero;

	Transform targeterTransform;
	Vector3 targeterEuler = Vector3.zero;

	Vector2 inputAsVector = Vector3.zero;

	public int playerArrayIndex;

	bool isMoving = false;
	public bool isFacingRight = true;
	RyansAwesomeSpritesheetAnimatorVersionTwoPointOh myAnim;

	public int numCandies = 0;
	public Transform candyHoverPos;

	RyanVineAdaptivePlayerTargetingMatrix myTargeter;
	int particleEmissionRate;
	ParticleSystem partsLeft, partsMid, partsRight;

	public bool hasTarget = false;

	public bool isAlive = true;

	// Use this for initialization
	void Start () 
	{
		horizInputString = "P" + playerNum.ToString() + "_Horizontal";
		vertInputString = "P" + playerNum.ToString() + "_Vertical";
		buttonString1 = "P" + playerNum.ToString() + "_Button1";
		buttonString2 = "P" + playerNum.ToString() + "_Button2";

		vineAnchor = GameObject.Find("vineAnchor" + playerNum.ToString()).rigidbody;

		myLine = GetComponent<LineRenderer>();
		myVineHandler = GetComponentInChildren<RyanVineVineHandler>();
		//aimTransform = transform.Find("Aiming");

		targeterTransform = transform.Find("Targeting");
		myTargeter = targeterTransform.GetComponent<RyanVineAdaptivePlayerTargetingMatrix>();

		myAnim = GetComponentInChildren<RyansAwesomeSpritesheetAnimatorVersionTwoPointOh>();
		myAnim.ChangeState(0);

		candyHoverPos = transform.Find("CandyHoverPos");

		partsLeft = transform.Find("EyeLaserLeft").particleSystem;
		partsMid = transform.Find("EyeLaserMid").particleSystem;
		partsRight = transform.Find("EyeLaserRight").particleSystem;

		//rigidbody.isKinematic = true;
		//maxVineDistance = Vector3.Distance(transform.position, cube.transform.position);
	}
	
	// Update is called once per frame
	void Update () 
	{
		//myVelo = rigidbody.velocity;

		if(isAlive)
		{
			xIn = Input.GetAxis(horizInputString);
			yIn = Input.GetAxis(vertInputString);
			inputAsVector.x = xIn;
			inputAsVector.y = yIn;
			//yIn = Input.GetAxis("P" + playerNum.ToString() + "_Vertical") * moveSpeed;
			
			//myVelo.x = xIn;
			//myVelo.y = yIn;

			ProcessAimDirection();

			xIn *= moveSpeed;
			yIn *= moveSpeed;

			float horizSpeed = Mathf.Abs(rigidbody.velocity.x);

			if(isMoving)
			{
				//myAnim.SetAnimSpeed(1, 0.05f * 
				float motionPercent = Mathf.Abs(rigidbody.velocity.x) / moveSpeed;
				float newSpeed = Mathf.Lerp(0.1f, 0.05f, motionPercent);
				myAnim.SetAnimSpeed(1, newSpeed);

				if(horizSpeed < 0.25f)
				{
					myAnim.ChangeState("idle");
					myTargeter.collider.enabled = false;
					isMoving = false;

					if(hasTarget)
					{
						partsLeft.Stop();
						partsLeft.Clear();
						partsMid.Play();
						partsRight.Stop();
						partsRight.Clear();
					}
				}

				if(isFacingRight)
				{
					if(rigidbody.velocity.x < 0)
					{
						myAnim.FaceLeft();
						isFacingRight = false;

						if(hasTarget)
						{
							partsLeft.Play();
							partsMid.Stop();
							partsMid.Clear();
							partsRight.Stop();
							partsRight.Clear();
						}
					}
				}
				else
				{
					if(rigidbody.velocity.x > 0)
					{
						myAnim.FaceRight();
						isFacingRight = true;

						if(hasTarget)
						{
							partsLeft.Stop();
							partsLeft.Clear();
							partsMid.Stop();
							partsMid.Clear();
							partsRight.Play();
						}
					}
				}
			}
			else
			{
				if(horizSpeed > 0.25f)
				{
					if(rigidbody.velocity.x < 0)
					{
						if(isFacingRight)
						{
							Debug.Log("Now facing left");
							myAnim.FaceLeft();
							isFacingRight = false;

							if(hasTarget)
							{
								partsLeft.Play();
								partsMid.Stop();
								partsMid.Clear();
								partsRight.Stop();
								partsRight.Clear();
							}
						}
						else
						{
							if(hasTarget)
							{
								partsLeft.Play();
								partsMid.Stop();
								partsMid.Clear();
								partsRight.Stop();
								partsRight.Clear();
							}
						}
					}
					else
					{
						if(!isFacingRight)
						{
							Debug.Log("Now facing right");
							myAnim.FaceRight();
							isFacingRight = true;

							if(hasTarget)
							{
								partsLeft.Stop();
								partsLeft.Clear();
								partsMid.Stop();
								partsMid.Clear();
								partsRight.Play();
							}
						}
						else
						{
							if(hasTarget)
							{
								partsLeft.Stop();
								partsLeft.Clear();
								partsMid.Stop();
								partsMid.Clear();
								partsRight.Play();
							}
						}
					}

					myAnim.ChangeState("running");
					myTargeter.collider.enabled = true;
					isMoving = true;
				}
			}

			//rigidbody.velocity = myVelo;


			if(isVined)
			{
				transform.position = vineAnchor.transform.position;

				vineAnchor.AddForce(xIn * airControl, 0, 0);

				Vector3.ClampMagnitude(vineAnchor.velocity, maxSpeed);

				myLine.SetPosition(1, transform.position);
				myLine.SetPosition(2, transform.position - (Vector3.up * remainingVineLength));

				//Debug.Log(vineAnchor.velocity.magnitude);
			}
			else
			{
				if(isGrounded)
				{
					myVelo = rigidbody.velocity;

					myVelo.x = xIn;
					rigidbody.velocity = myVelo;


				}
				else
					rigidbody.AddForce(xIn * airControl, 0, 0);
			}


			if(Input.GetButtonDown(buttonString1))
			{
				if(isVined)
					DetachVine();
				else
				{
					if(isGrounded)
						Jump();
					else if(hasVineNearby)
						AttachVine();
				}
			}

			//Debug.Log(isGrounded + " " + Time.timeSinceLevelLoad);
		}
	}

	void ProcessAimDirection()
	{
		if(inputAsVector.magnitude > 0.75f)
		{	
			float angle = Mathf.Atan2(yIn, xIn);
			angle *= Mathf.Rad2Deg;
			//Debug.Log(angle);

			float quotient = angle / 45.0f;
			float rounded = Mathf.RoundToInt(quotient);

			angle = rounded * 45f;

			targeterEuler.z = angle;
			targeterTransform.localEulerAngles = targeterEuler;
			targeterTransform.position = transform.position;
			targeterTransform.Translate(8.5f, 0.0f, 0.0f);
		}
	}

	public void MakeGrounded()	{	isGrounded = true;	}
	public void MakeUngrounded()	{	isGrounded = false;	}

	public void GetVineNearby(RyanVineVine vineToGet)	{	hasVineNearby = true;	vineNearby = vineToGet;	}
	public void LoseVineNearby()	{	hasVineNearby = false;	vineNearby = null;	}

	public void DetachVine()	
	{	
		isVined = false;
		rigidbody.isKinematic = false;
		rigidbody.velocity = vineAnchor.velocity;

		vineNearby.DetachPlayer();

		vineAnchor.isKinematic = true;

		myLine.enabled = false;

		LoseVineNearby();
		myVineHandler.LoseVineInfo();
	}

	public void AttachVine()
	{
		vineAnchor.isKinematic = false;

		vineAnchor.transform.position = transform.position;
		vineAnchor.velocity = rigidbody.velocity;
		vineNearby.AttachPlayerToSelf(vineAnchor, this);

		isVined = true;
		rigidbody.isKinematic = true;

		myLine.SetPosition(0, vineJointPos);
		myLine.enabled = true;
	}

	public void GetVineJointPosition(Vector3 pos, float totalVineLength)	
	{	
		vineJointPos = pos;	

		float dist = Vector3.Distance(transform.position, vineJointPos);

		remainingVineLength = totalVineLength - dist;
		//Debug.Log("dist: " + dist + " || " + remainingVineLength + " vine hanging");
	}

	void Jump()
	{
		myVelo = rigidbody.velocity;

		myVelo.y = jumpSpeed;
		
		rigidbody.velocity = myVelo;
	}

	public void AddCandy()
	{
		numCandies++;
		myTargeter.UpdateSelfCandies(this.numCandies);
	}

	public void SetStatus(RyanVinePlayer otherPlayer)
	{
		this.numCandies = otherPlayer.numCandies;
		myTargeter.UpdateSelfCandies(this.numCandies);
	}

	public void SetParticleEmission(int newEmissionRate)
	{
		particleEmissionRate = newEmissionRate;

	}

	public void FuckingDieAlready()
	{
		isAlive = false;
	}
}
