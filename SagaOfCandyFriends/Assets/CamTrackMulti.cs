﻿using UnityEngine;
using System.Collections;

public class CamTrackMulti : MonoBehaviour {
	GameObject[] players;
	public string playerTag = "Monkey";
	public Vector3 camOffsetFromCenter = new Vector3(0.0f, 0.0f, -23.0f);
	public float targetRadiusToDist = 1.0f; //Tweak to map your cam FOV to target cluster radius
	private Vector3 vel = Vector3.zero;
	public float smoothMove = 0.1f; //Increase for smoother camera moves at the cost of lagging fast moving targets.
	 
	//This tracks multiple targets and moves the camera so they fill the screen.
	//IDEA: bias percentage, ie mostly show stuff above.  Only good for single player?
	public float minDist = 1.0f; //This comes into play when tracking a single object like the player
	public float maxDist = 900.0f; //Set this less than your far clip, but really big so you know when things go wrong, no smarts here
									//IDEA: prioritize objects, start ignoring until you are back in range.  Less useful for multiplayer, we can just ignore candy.
	// Use this for initialization
	void Start () {
		players = GameObject.FindGameObjectsWithTag (playerTag);
	}
	
	// Update is called once per frame
	void Update () {
		hackyMathTracking ();
	}

	void hackyMathTracking ()
	{
		Vector3 min = Vector3.zero;
		Vector3 max = Vector3.zero;
		foreach (GameObject p in players) {
			Vector3 pos = p.transform.position;
			//Debug.Log("Cam p pos:" + pos);
			min = Vector3.Min (min, pos);
			max = Vector3.Max (max, pos);
		}
		Vector3 targetWorld = (min + max) / 2;
		float targetsRadius = 0.0f;
		foreach (GameObject p in players) {
			Vector3 pos = p.transform.position;
			targetsRadius = Mathf.Max (targetsRadius, Mathf.Abs (Vector3.Distance (targetWorld, pos)));
		}
		Vector3 targetViewPort = camera.WorldToViewportPoint (targetWorld);
		Vector3 delta = targetWorld - camera.ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, targetWorld.z));
		Vector3 dest = transform.position + delta + camOffsetFromCenter;
		dest.z = targetRadiusToDist * targetsRadius;
		//
		transform.position = Vector3.SmoothDamp (transform.position, dest, ref vel, smoothMove);
//		Debug.Log ("Cam p cur:" + transform.position + " target:" + targetWorld + " min:" + min + " max:" + max);
	}

	//If you want to do this right
	void goodMath()
	{
		//http://docs.unity3d.com/Documentation/Manual/FrustumSizeAtDistance.html
		//		The height of the frustum at a given distance (both in world units) can be obtained with the following formula:-
		//			var frustumHeight = 2.0 * distance * Mathf.Tan(camera.fieldOfView * 0.5 * Mathf.Deg2Rad);
		//		
		//		...and the process can be reversed to calculate the distance required to give a specified frustum height:-
		//			var distance = frustumHeight * 0.5 / Mathf.Tan(camera.fieldOfView * 0.5 * Mathf.Deg2Rad);
		//		
		//		It is also possible to calculate the FOV angle when the height and distance are known:-
		//			var camera.fieldOfView = 2 * Mathf.Atan(frustumHeight * 0.5 / distance) * Mathf.Rad2Deg;
		//		
		//		Each of these calculations involves the height of the frustum but this can be obtained from the width (and vice versa) very easily:-
		//			var frustumWidth = frustumHeight * camera.aspect;
		//			var frustumHeight = frustumWidth / camera.aspect;
	}	
}
