﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct InputSet
{
	public float xIn;
	public float yIn;
	public bool action1;
	public bool action2;
}

public class StateManager : MonoBehaviour {

	#region Variables

	public bool hasInit = false;
	public bool levelInit = false;

	[HideInInspector]
	public LevelManager levelManager = null;

	public int minPlayers = 2;
	public int maxPlayers = 4;
	public PlayerHolder[] players = new PlayerHolder[0];

	#endregion

	#region Init

	void Awake()
	{
		DontDestroyOnLoad(this);
	}

	public void InitManager(LevelManager lManager)
	{
		players = new PlayerHolder[maxPlayers];
		for(int i=0;i<maxPlayers;i++)
		{
			players[i] = new PlayerHolder();
			players[i].playerSlotIndex = i;
		}

		levelManager = lManager;
		hasInit = true;
	}

	public void InitLevelLoad(LevelType lvlType)
	{
		if(!hasInit)
		{
			Debug.LogWarning("StateManager !hasInit, something is wrong.");
		}

		levelInit = true;

		//TODO - Set up a switch to init the state manager based on various level types

		foreach(PlayerHolder holder in players)
		{
			holder.currentScore = 0;
		}
	}

	#endregion

	#region Players

	public void ResetPlayer(int index)
	{
		players[index] = new PlayerHolder();
	}

	public bool SlotsFilled()
	{
		foreach(PlayerHolder holder in players)
		{
			if(!holder.playerSlotFilled)
				return false;
		}

		return true;
	}

	#endregion

	#region Inputs

	public InputSet CheckInputs(int index)
	{
		InputSet set = new InputSet();
		set.xIn = Input.GetAxis("P" + index + "_Horizontal");
		set.yIn = Input.GetAxis("P" + index + "_Vertical");

		set.action1 = Input.GetButtonDown("P" + index + "_Button1");
		set.action2 = Input.GetButtonDown("P" + index + "_Button2");
		return set;
	}

	public void WatchForInputs()
	{
		for(int i=1;i<=6;i++)
		{
			InputSet set = CheckInputs(i);
			if(set.action1 && InputIndexUnused(i))
			{
				Debug.Log ("Action pressed");
				SetIndexForOpenSlot(i);
				if(SlotsFilled())
					return;
			}
		}
	}

	bool InputIndexUnused(int index)
	{
		for(int i=0;i<players.Length;i++)
		{
			if(players[i].playerSlotFilled)
			{
				if(players[i].playerInputIndex == index)
					return false;
			}
		}

		return true;
	}

	void SetIndexForOpenSlot(int inputIndex)
	{
		for(int i=0;i<players.Length;i++)
		{
			if(!players[i].playerSlotFilled)
			{
				players[i].AssignPlayerInputIndex(inputIndex);
				return;
			}
		}
	}

	#endregion

	#region Levels

	public void SwitchingToNewLevel()
	{
		levelInit = false;
	}

	#endregion
}