﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Chipmunk : PlayeryThing {

	#region Init

	public List<Candy> candyHeldList = new List<Candy>();			//All candy objects the player is currently holding onto

	private bool isStunned = false;									//Chipmunks get stunned when hit with a bullet, cheap flag during event
	private float stunStartTime = 0f;								//Records the start time of the stun
	public float stunDuration = 5f;									//Duration of the stun, public and set in editor, can be set by a bullet if we wish

	public List<Bullet> bulletHeldList = new List<Bullet>();
	public int maxBulletCount = 2;


	public float throwSpeed = 50f;

	public float eatingSpeed = 10f;

	public FourDirections currentDirection = FourDirections.Up;

	public Vector2 upThreshold = new Vector2(-45f, 45f);
	public Vector2 leftThreshold = new Vector2(-45f, -135f);
	public Vector2 rightThreshold = new Vector2(45f, 135f);

	public Transform muzzleRoot = null;

	#endregion

	#region Inputs

	public override void Action1Pressed ()
	{
		Debug.Log ("Chipmunk action 1 pressed");
		if(bulletHeldList.Count > 0 && !isStunned)
		{
			ThrowBullet(bulletHeldList[0]);
		}
	}

	public override void Action2Pressed ()
	{
		Debug.Log ("Chipmunk action 2 pressed");
	}

	#endregion

	#region Actions

	public AudioClip[] bulletPickupCLips = new AudioClip[0];
	public AudioSource bulletPickupSource = null;

	void PickUpBullet(Bullet newBullet)
	{
		Debug.Log ("Trying to pick up the spear");
		if(bulletHeldList.Count >= maxBulletCount)
			return;

		if(bulletHeldList.Contains(newBullet))
			Debug.LogWarning("Bullet already in bullet held list, why?");

		AudioClip randomClip = bulletPickupCLips[Random.Range(0, bulletPickupCLips.Length - 1)];
		bulletPickupSource.PlayOneShot(randomClip);

		LoseCandy();

		bulletHeldList.Add(newBullet);

		levelManager.UpdateBulletRef(bulletHeldList.Count, holder);

		Debug.Log ("Got me a bullet");
		newBullet.PickedUp(this);
		newBullet.xForm.parent = xForm;
		newBullet.xForm.position = xForm.position;
		newBullet.xForm.rotation = xForm.rotation;
	}

	void ThrowBullet(Bullet bulletToToss)
	{
		if(Physics.CheckSphere(muzzlePoint.position, 0.2f))
		{
			Debug.Log ("Can't throw an acorn while up against a wall");
			return;
		}

		Debug.Log("Tossing spear");
		//Launch the spear from the muzzle point
		bulletToToss.xForm.position = muzzlePoint.position;
		bulletToToss.xForm.rotation = muzzlePoint.rotation;
		bulletToToss.xForm.parent = null;

		//Tell the bullet it was thrown, this sets flags and enables the renderer
		bulletToToss.Thrown(this);

		//Get the spear moving, call this after thrown or the held bullet will be set to kinematic
		bulletToToss.SetVelocity(muzzlePoint.forward * throwSpeed);

		//Remove the bullet from the list of bullets this chipmunk has
		bulletHeldList.Remove(bulletToToss);

		levelManager.UpdateBulletRef(bulletHeldList.Count, holder);
	}

	void LoseBullets()
	{
		Debug.Log ("Losing all meh bullets.");
		foreach(Bullet bullet in bulletHeldList)
		{
			bullet.xForm.parent = null;
			bullet.xForm.position = xForm.position;
			bullet.col.enabled = true;
			bullet.rigid.isKinematic = false;
			bullet.holder = null;
			bullet.isHeld = false;
			bullet.isDangerous = false;
			bullet.canPickUp = false;
			bullet.lastThrownTime = Time.time;
			bullet.ShowBullet();
		}

		bulletHeldList = new List<Bullet>();
		levelManager.UpdateBulletRef(0, holder);
	}

	#endregion

	#region Candy

	void UpdateCandyEating()
	{
		float eaten = Time.fixedDeltaTime * eatingSpeed;
		Candy[] tempList = new Candy[candyHeldList.Count];
		candyHeldList.CopyTo(tempList);
		foreach(Candy treat in tempList)
		{
			treat.CandyCapacityEaten(eaten);
			if(treat.isEaten)
			{
				candyHeldList.Remove(treat);
				CandyCompletelyEaten(treat);
			}
		}
		levelManager.UpdateCandyRef(candyHeldList.Count, holder);
	}

	void ShowEatingProgress()
	{
		progressBarBack.enabled = true;
		progressBarPerc.enabled = true;
	}

	void HideEatingProgress()
	{
		progressBarBack.enabled = false;
		progressBarPerc.enabled = false;
	}

	void UpdateProgressBarPerc()
	{
		Vector3 newScale = progressBarXForm.localScale;

		if(candyHeldList.Count > 0)
		{
			newScale.x = candyHeldList[0].GetEatingPercent();
		}

		progressBarXForm.localScale = newScale;
	}

	public AudioClip[] swallowedClips = new AudioClip[4];
	public AudioSource swallowedSource = null;

	public void CandyCompletelyEaten(Candy candyEaten)
	{
		AudioClip randomClip = swallowedClips[Random.Range(0, swallowedClips.Length - 1)];
		swallowedSource.PlayOneShot(randomClip);

		levelManager.CandyEaten(holder, candyEaten);
		holder.displayRefs.textInfo.text = holder.currentScore.ToString();
		candyEaten.CleanUpCandy();
		if(candyHeldList.Count <= 0)
		{
			HideEatingProgress();
			chewSource.Stop();
		}
	}

	public AudioSource chewSource = null;

	public void GotCandy(Candy candy)
	{
		if(candyHeldList.Count >= maxCandyCount)
			return;

		chewSource.Play();
		ShowEatingProgress();
		LoseBullets();
		candyHeldList.Add(candy);
		candy.CandyPickedUp(this);
		levelManager.UpdateCandyRef(candyHeldList.Count, holder);
		foreach(Candy treat in candyHeldList)
		{
			treat.ResetCandyCapacity();
		}
	}

	public AudioClip[] vomitClips = new AudioClip[4];
	public AudioSource vomitSource = null;

	public void LoseCandy()
	{
		foreach(Candy candy in candyHeldList)
		{
			candy.CandyDropped(xForm.position);
		}
		candyHeldList = new List<Candy>();

		levelManager.UpdateCandyRef(0, holder);
		AudioClip randomClip = vomitClips[Random.Range(0, vomitClips.Length - 1)];
		vomitSource.PlayOneShot(randomClip);
		HideEatingProgress();
		chewSource.Stop();
	}

	#endregion

	#region Stunning

	public AudioClip[] stunClips = new AudioClip[6];
	public AudioSource stunSource = null;

	public AudioClip[] bodyHitClips = new AudioClip[0];
	public AudioSource bodyHitSource = null;

	public void StartStun()
	{
		AudioClip randomClip = stunClips[Random.Range(0, stunClips.Length - 1)];
		stunSource.PlayOneShot(randomClip);
		anim.SetBool("isHurt", true);
		isStunned = true;
		stunStartTime = Time.time;
		Debug.Log ("TODO - Any art things we need to call when the player gets stunned? " + Time.time);
	}

	public void SetStunDuration (float newDuration)
	{
		stunDuration = newDuration;
	}

	void StunEnded()
	{
		isStunned = false;
		anim.SetBool("isHurt", false);
		Debug.Log ("Art, sound, notifications here? " + Time.time);
	}

	#endregion

	#region Updates

	public override void ProcessInputs (InputSet newSet)
	{
		if(isStunned)
			return;

		base.ProcessInputs (newSet);

		UpdateDirection(newSet);
	}

	void UpdateDirection(InputSet newSet)
	{
		if(Mathf.Abs(newSet.xIn) + Mathf.Abs(newSet.yIn) <= levelManager.directionDeadZone)
			return;

		float angle = Mathf.Atan2(newSet.xIn, newSet.yIn) * Mathf.Rad2Deg;

		Vector3 rot = muzzleRoot.rotation.eulerAngles;
		//Less than 45 && above -45
		if(angle <= upThreshold.y && angle >= upThreshold.x)
		{
			//Direction is Up
			currentDirection = FourDirections.Up;
			rot.y = 0f;
		}
		//Above 45 && less than 135
		else if(angle > rightThreshold.x && angle <= rightThreshold.y)
		{
			//Direction is right
			currentDirection = FourDirections.Right;
			rot.y = 90f;
		}
		//Above -135 and less than -45
		else if(angle >= leftThreshold.y && angle < leftThreshold.x)
		{
			//Direction is left
			currentDirection = FourDirections.Left;
			rot.y = 270f;
		}
		//Defaults to less than -135 and greater than 135,
		else
		{
			//Direction is down
			currentDirection = FourDirections.Down;
			rot.y = 180f;
		}

		muzzleRoot.rotation = Quaternion.Euler(rot);
		anim.SetInteger("direction", (int)currentDirection);
	}

	public override void Update ()
	{
		base.Update();

		if(candyHeldList.Count > 0)
		{
			UpdateProgressBarPerc();
		}

		if(Input.GetKeyDown(KeyCode.Alpha1))
			anim.SetInteger("direction", 0);

		if(Input.GetKeyDown(KeyCode.Alpha2))
			anim.SetInteger("direction", 1);

		if(Input.GetKeyDown(KeyCode.Alpha3))
			anim.SetInteger("direction", 2);

		if(Input.GetKeyDown(KeyCode.Alpha4))
			anim.SetInteger("direction", 3);

		if(Input.GetKeyDown(KeyCode.Alpha5))
			anim.SetBool("isHurt", true);

		if(Input.GetKeyDown(KeyCode.Alpha6))
			anim.SetBool("isHurt", false);
	}

	public override void FixedUpdate ()
	{
		if(!isActive)
			return;

		if(isStunned)
		{
			if(Time.time >= stunStartTime + stunDuration)
			{
				StunEnded();
			}
		}
		else
		{
			if(candyHeldList.Count > 0)
			{
				UpdateCandyEating();
			}
		}
	}

	#endregion

	#region Collisions

	void OnTriggerEnter(Collider other)
	{
//		Debug.Log ("Trigger entered. Other: " + other.transform.root.name + " Host: " + go.name);
		if(other.tag == "Candy")
		{
			Candy candy = other.transform.root.GetComponent<Candy>();
			if(candy.canBePickedUp && !candy.isHeld && !isStunned)
			{
				GotCandy(candy);
			}
		}
	}

	void OnCollisionEnter(Collision other)
	{
//		Debug.Log ("Collision event. Other: " + other.gameObject.name + " Host: " + go.name);
		if(other.gameObject.tag == "Bullet")
		{
			Bullet bullet = other.gameObject.GetComponent<Bullet>();
			if(bullet.isDangerous)
			{
				HitByBullet(bullet);
			}
			else if(bullet.canPickUp && !isStunned)
			{
				PickUpBullet(bullet);
			}
		}
	}

	public void HitByBullet(Bullet bullet)
	{
		LoseCandy();
		StartStun();
		bullet.CleanupBullet();
	}

	#endregion
}