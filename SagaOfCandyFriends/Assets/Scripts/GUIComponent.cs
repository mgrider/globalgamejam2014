﻿using UnityEngine;
using System.Collections;

public class GUIComponent : MonoBehaviour {

	public LevelManager levelManager = null;

	public Vector2[] playerSlotPositions = new Vector2[6];

	public Vector2 scoreBoxSize = new Vector2(100f, 50f);
	public Vector2 spacingValues = new Vector2(100f, 64f);

	public GUISkin gSkin = null;

	void Awake()
	{
		playerSlotPositions = new Vector2[6];
		playerSlotPositions[0] = spacingValues;
		playerSlotPositions[1] = new Vector2(Screen.width - spacingValues.x - 100f, spacingValues.y);
		playerSlotPositions[2] = new Vector2(Screen.width - spacingValues.x - 100f, Screen.height - spacingValues.y - 50f);
		playerSlotPositions[3] = new Vector2(spacingValues.x, Screen.height - spacingValues.y - 50f);
		playerSlotPositions[4] = new Vector2(spacingValues.x, (Screen.height * 0.5f) - (scoreBoxSize.y * 0.5f));
		playerSlotPositions[5] = new Vector2(Screen.width - spacingValues.x - 100f, (Screen.height * 0.5f) - (scoreBoxSize.y * 0.5f));
	}
}
