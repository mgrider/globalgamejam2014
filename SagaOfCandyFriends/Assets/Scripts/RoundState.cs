﻿using UnityEngine;
using System.Collections;

public enum RoundState
{
	Waiting,
	Playing,
	Finished,
	Loading,
	Choosing,
	ERROR
}