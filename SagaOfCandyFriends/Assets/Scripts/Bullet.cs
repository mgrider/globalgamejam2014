﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	#region Variables

	public LevelManager levelManager = null;		//Reference to the level manager, set by the level manager on projectile creation

	public Transform xForm = null;					//Reference to the transform component
	public Renderer[] rends = new Renderer[0];		//Refernce to the renderers of this object
	public GameObject go = null;					//Reference to the GameObject component
	public Rigidbody rigid = null;					//Reference to the Rigidbody component
	public Collider col = null;

	public Transform centerOfGravity = null;		//Center of gravity transform

	[HideInInspector]
	public PlayeryThing holder = null;				//Reference to the holder when bullet is being held
	[HideInInspector]
	public bool isHeld = false;						//Flag when bullet is being held
	[HideInInspector]
	public PlayeryThing lastThrower = null;			//Reference to the person who threw the bullet
//	[HideInInspector]
	public bool canPickUp = false;					//Flag to determine when a bullet can be picked up

//	[HideInInspector]
	public bool isDangerous = false;				//When thrown, the bullet is dangerous until it drops below a speed threshold

	[HideInInspector]
	public float lastThrownTime = 0f;				//Time when last throw occurred, ties in with pickup delay
	public float pickupDelayTime = 5f;				//Seconds after last throw until the bullet can be picked up again

	public bool isActive = false;					//Flagged true after init

	public float safeSpeed = 0.1f;					//velocity threshold where a bullet is no longer dangerous

	#endregion

	#region Init

	void Awake()
	{
		if(xForm == null)
		{
			Debug.LogWarning("Bullet xForm is null");
			xForm = transform;
		}
		if(go == null)
		{
			Debug.LogWarning("Bullet GO is null");
			go = gameObject;
		}
		if(rigid == null)
		{
			Debug.LogWarning("Bullet Rigid is null");
			rigid = rigidbody;
		}
		if(centerOfGravity == null)
		{
			Debug.LogError("Center of gravity not set on bullet.");
			centerOfGravity = xForm;
		}
		if(rends.Length <= 0)
		{
			Debug.LogError("Bullet Rends are null, set these");
			return;
		}
	}

	public void InitBullet(LevelManager lManager)
	{
		levelManager = lManager;
		isActive = true;
		rigid.centerOfMass = centerOfGravity.localPosition;
	}

	#endregion

	#region Players

	public void SetHolder(PlayeryThing newHolder)
	{
		holder = newHolder;
	}

	public void SetLastThrower(PlayeryThing last)
	{
		lastThrower = last;
	}

	#endregion 

	#region Visibility

	public void ShowBullet()
	{
		Debug.Log ("Showing bullet");
		col.enabled = true;
		foreach(Renderer rend in rends)
		{
			rend.enabled = true;
		}
	}

	public void HideBullet()
	{
		Debug.Log ("Hiding bullet");
		col.enabled = false;
		foreach(Renderer rend in rends)
		{
			rend.enabled = false;
		}
	}

	#endregion

	#region Events

	public void PickedUp(PlayeryThing newHolder)
	{
		SetHolder(newHolder);
		HideBullet();
		col.enabled = false;
		rigid.isKinematic = true;
		isHeld = true;
		canPickUp = false;
		isDangerous = false;
	}

	public void Thrown(PlayeryThing last)
	{
		Debug.Log ("I'm a bullet, I just got thrown.");
		SetLastThrower(last);
		col.enabled = true;
		rigid.isKinematic = false;
		holder = null;
		isHeld = false;
		isDangerous = true;
		canPickUp = false;
		lastThrownTime = Time.time;
		ShowBullet();
	}

	void PickMeUpNow()
	{
		canPickUp = true;
		isDangerous = false;
		Debug.Log ("TODO - Does art need to change to indicate when the bullet can be picked up again?");
	}

	void NoLongerDangerous()
	{
		isDangerous = false;
	}

	void MAMAWeJustKilledAMan()
	{
		lastThrower.MAMAWeJustKilledAMan();
	}

	public void SetVelocity(Vector3 newVelocity)
	{
		Debug.Log ("Bullet is getting velocity: " + newVelocity);
		rigid.velocity = newVelocity;
	}

	public void CleanupBullet()
	{
		levelManager.LostBullet();
		Destroy(gameObject);
	}

	#endregion

	#region Updates

	void FixedUpdate()
	{
		//If the bullet is not being held and it can't be picked up, update the timer until it can be
		if(!isHeld && !canPickUp)
		{
			if(Time.time >= lastThrownTime + pickupDelayTime)
			{
				//Enough time has passed, flag this so people can pick it up
				PickMeUpNow();
			}
		}

		if(isDangerous)
		{
			if(rigid.velocity.magnitude <= safeSpeed)
				NoLongerDangerous();
		}
	}

	#endregion
}
