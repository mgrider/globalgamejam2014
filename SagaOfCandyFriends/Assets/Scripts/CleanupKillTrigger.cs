﻿using UnityEngine;
using System.Collections;

public class CleanupKillTrigger : MonoBehaviour {

	public LevelManager levelManager = null;

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Player")
		{
			PlayeryThing playerScript = other.GetComponent<PlayeryThing>();
			playerScript.FellOffWorld();
		}
		else if(other.tag == "Candy")
		{
			Candy candyScript = other.GetComponent<Candy>();
			levelManager.LostCandyPiece();
			candyScript.CleanUpCandy();
		}
		else if(other.tag == "Bullet")
		{
			Bullet bullet = other.GetComponent<Bullet>();
			bullet.CleanupBullet();
			levelManager.LostBullet();
		}
	}
}