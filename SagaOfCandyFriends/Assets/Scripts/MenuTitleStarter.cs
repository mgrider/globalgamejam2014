﻿using UnityEngine;
using System.Collections;

public class MenuTitleStarter : MonoBehaviour 
{
	private bool movingAway = false;

	// Use this for initialization
	void Start () 
	{
		transform.position = Vector3.zero;
		Invoke ("MoveAway", 5);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void MoveAway()
	{
		if(!movingAway)
		{
			movingAway = true;
			iTween.MoveTo(gameObject, iTween.Hash ("y", 10f, "time", 0.5f, "easetype", iTween.EaseType.easeInBack));
		}
	}

	void OnMouseUpAsButton()
	{
		MoveAway();
	}
}
