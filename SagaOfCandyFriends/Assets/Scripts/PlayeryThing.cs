﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayeryThing : MonoBehaviour {

	#region Variables

	public Transform xForm = null;						//Reference to Transform of the player, saves a component lookup for every call
	public GameObject go = null;						//Reference to GameObject of the player, saves a component lookup for every call
	public Collider col = null;							//Reference to Collider of the player, saves a component lookup for every call
	public Rigidbody rigid = null;						//Reference to rigidbody of the player, saves a component lookup for every call

	public Animator anim = null;

	public float moveSpeed = 4f;						//Movement speed, multiplied by the input axis to set velocity in m/sec

	public GameObject bulletPrefab = null;				//Reference to a prefab if we want to create one like a bullet
	public Transform muzzlePoint = null;				//Point to launch a projectile from, whether bullet, acorn, spear or rainbow

	[HideInInspector]
	public LevelManager levelManager = null;			//Set on the Init call when player is instantiated

	protected PlayerHolder holder = null;					//Reference to the holder object that ties into multiplayer and score tracking

	public bool hasInit = false;
	public bool isActive = false;

	public int maxCandyCount = 3;

	public SpriteRenderer chippySprite = null;
	public Renderer progressBarBack = null;
	public Renderer progressBarPerc = null;
	public Transform progressBarXForm = null;

	#endregion

	#region Init

	public virtual void Awake()
	{
		if(xForm == null)
		{
			Debug.LogWarning("xForm is null on object " + gameObject.name);
			xForm = transform;
		}

		if(go == null)
		{
			Debug.LogWarning("GO is null on object: " + gameObject.name);
			go = gameObject;
		}

		if(rigid == null)
		{
			Debug.LogWarning("Rigid is null on object: " + gameObject.name);
			rigid = rigidbody;
		}
	}

	public virtual void InitPlayeryThing(LevelManager lManager, PlayerHolder newHolder, Color newTint)
	{
		levelManager = lManager;
		hasInit = true;
		isActive = true;
		holder = newHolder;
		chippySprite.color = newTint;
	}

	#endregion

	#region Updates

	public virtual void Update()
	{
		if(!isActive)
			return;

		//Pull a struct of inputs, x/y axis and 2 buttons
		InputSet newSet = levelManager.manager.CheckInputs(holder.playerInputIndex);

		holder.lastSet = newSet;

		//Do stuff with the inputs
		ProcessInputs(newSet);
	}

	public virtual void FixedUpdate()
	{
		//
	}

	#endregion

	#region Inputs

	//Makes stuff move and do things
	public virtual void ProcessInputs(InputSet newSet)
	{
		Vector3 newVelocity = new Vector3(newSet.xIn * moveSpeed, 0f, newSet.yIn * moveSpeed);
		SetNewVelocity(newVelocity);

		if(newSet.action1)
			Action1Pressed();

		if(newSet.action2)
			Action2Pressed();
	}

	//Basic action call on button press, overload this in each of the avatar types that do different things
	public virtual void Action1Pressed()
	{
		Debug.Log ("Action1Pressed - Override me in the player thingy this is.");
	}

	//Basic action call on button press, overload this in each of the avatar types that do different things
	public virtual void Action2Pressed()
	{
		Debug.Log ("Action2Pressed - Override me in the player thingy this is.");
	}

	#endregion

	#region Movement

	//Basic velocity setting call, doesn't use forces just brute assignment
	public virtual void SetNewVelocity(Vector3 vel)
	{
		rigid.velocity = vel;
	}

	#endregion

	#region Events

	//Example firing method if we wanted to create and launch a projectile
	void FireGun()
	{
		GameObject newBullet = (GameObject)Instantiate(bulletPrefab, muzzlePoint.position, muzzlePoint.rotation);
		
		Bullet bulletScript = newBullet.GetComponent<Bullet>();
		
		bulletScript.SetLastThrower(this);

		//Do physicsy stuff here like add forces or set initial velocities
	}

	//Example method to show calls up the tree for an event
	public void MAMAWeJustKilledAMan()
	{
		float amount = 0;
		
		//If we killed a gerbil
		amount = 20;
		
		//If we killed a mountain giant
		amount = 100;
		
		levelManager.MAMAWeJustKilledAMan(amount);
	}

	public void RoundFinished()
	{
		isActive = false;
	}

	public void FellOffWorld()
	{
		Transform spawnPoint = levelManager.GetPlayerSpawnPoint(holder);
		xForm.position = spawnPoint.position;
		xForm.rotation = spawnPoint.rotation;
		rigid.velocity = Vector3.zero;
		rigid.angularVelocity = Vector3.zero;
	}

	#endregion
}
