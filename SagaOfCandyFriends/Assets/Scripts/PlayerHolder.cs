﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PlayerHolder
{
	public int playerSlotIndex = 0;						//Which flying chipmunk is being driven
	public int playerInputIndex = 0;					//
	public bool playerSlotFilled = false;				//

	public PlayeryThing avatar = null;					//Reference to the chipmunk

	public float currentScore = 0f;

	public InputSet lastSet = new InputSet();

	public GUIDisplayRefs displayRefs = null;

	public void AssignPlayerInputIndex(int input)
	{
		if(playerSlotFilled)
			return;

		playerInputIndex = input;
		playerSlotFilled = true;
	}

	public void AddScore(float amount)
	{
		currentScore += amount;
	}

	public void RoundFinished()
	{
		if(avatar != null)
			avatar.RoundFinished();
	}
}