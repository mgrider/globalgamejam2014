﻿using UnityEngine;
using System.Collections;

public enum FourDirections
{
	Up,
	Down,
	Left,
	Right
}
