﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour {

	#region Variables

	public LevelType levelType = LevelType.Menu;
	public RoundState roundState = RoundState.Waiting;

	public GUIComponent gui = null;						//Reference to the GUI object in the scene

	public GameObject stateManagerPrefab = null;		//Reference to the state manager prefab in the project for instantiation
	public StateManager manager = null;					//Reference to the state manager currently active in the scene

	public GameObject genericPlayeryThingPrefab = null;	//Player prefab, generic, everybody gets the same thing
	[HideInInspector]
	public List<Bullet> bulletList = new List<Bullet>();
	public List<Candy> candyList = new List<Candy>();
	public List<Transform> objectSpawnPoints = new List<Transform>();

	public int spearCountMax = 2;
	public int candyCountMax = 2;

	public int startingBullets = 5;

	public float cheekyMaxScore = 5f;

	public float candyRespawnDelay = 3f;

	public bool playActive = false;
	public bool scoreChanged = false;

	public AvatarSpawnPoint[] avatarSpawnPoints = new AvatarSpawnPoint[0];

	public GameObject chipmunkAvatarPrefab = null;

	public GameObject candyPiecePrefab = null;
	public GameObject spearPrefab = null;
	public GameObject guiDisplayRefPrefab = null;

	public float directionDeadZone = 0.2f;

	public Sprite[] chippyStatusSprites = new Sprite[3];
	public Sprite[] bulletStatusSprites = new Sprite[3];
	public Sprite[] playerBackgroundSprites = new Sprite[6];
	public Vector3[] displayRefPositions = new Vector3[6];
	public Texture[] progressBarTextures = new Texture[6];
	public Color[] chippyTintColors = new Color[6];

	public AudioSource candyArrivalSource = null;
	public AudioClip[] candyArrivalClips = new AudioClip[0];

	#endregion

	#region Init

	void OnLevelWasLoaded()
	{
		//This fires between Awake and Start, just call reference to StateManager in start
	}

	void Start()
	{
		//Does a state manager already exist?
		GameObject foundObject = GameObject.FindGameObjectWithTag("StateManager");
		if(foundObject == null)
		{
			//Nope, create a new state manager object
			foundObject = (GameObject)Instantiate(stateManagerPrefab, Vector3.zero, Quaternion.identity);
		}

		//State manager object exists, either already or from instantiation, grab the component off of it
		manager = foundObject.GetComponent<StateManager>();

		//Initialize the state manager if it hasn't been already
		if(!manager.hasInit)
			manager.InitManager(this);

		//Initialize the level, make stuff spawn, set up characters, send RPCs etc.
		InitLevel();
	}

	void InitLevel()
	{
		manager.InitLevelLoad(levelType);

		switch((int)levelType)
		{
			case (int)LevelType.Chipmunks:
			{
				InitChipmunks();
				if(SlotsFilled())
					StartChipmunkPlay();
				else
					roundState = RoundState.Waiting;
				playActive = true;
				break;
			}
			case (int)LevelType.Birdies:
			{
				InitBirdies();
				if(SlotsFilled())
					StartBirdiesPlay();
				else
					roundState = RoundState.Waiting;
				playActive = true;
				break;
			}
			case (int)LevelType.Menu:
			{
				InitMenu();
				roundState = RoundState.Waiting;
				break;
			}
			default:
			{
				Debug.LogError("Bad Level Type on init: " + levelType);
				roundState = RoundState.ERROR;
				playActive = false;
				break;
			}
		}
	}

	void StartGameType(LevelType lType)
	{
		if(levelType == LevelType.Chipmunks)
			StartChipmunkPlay();
		else if(levelType == LevelType.Birdies)
			StartBirdiesPlay();

	}

	bool SlotsFilled()
	{
		return manager.SlotsFilled();
	}

	void InitChipmunks()
	{
//		CreateCandyPieces();
		List<Transform> tempSpawnList = objectSpawnPoints;
		Transform candySpawn1 = tempSpawnList[Random.Range(0, tempSpawnList.Count - 1)];
		tempSpawnList.Remove(candySpawn1);
		Transform candySpawn2 = tempSpawnList[Random.Range(0, tempSpawnList.Count - 1)];
		tempSpawnList.Remove(candySpawn2);

		CreateCandyPiece(candySpawn1);
		CreateCandyPiece(candySpawn2);

		for(int i=0;i<startingBullets;i++)
		{
			int rand = Random.Range(0, tempSpawnList.Count - 1);
			Transform spawnPoint = tempSpawnList[rand];
			CreateBullet(spawnPoint);
			tempSpawnList.Remove(spawnPoint);
		}

		CreateGUIDisplayRefs();
	}

	void CreateGUIDisplayRefs()
	{
		foreach(PlayerHolder holder in manager.players)
		{
			int index = holder.playerSlotIndex;
			GameObject guiDispObj = (GameObject)Instantiate(guiDisplayRefPrefab, displayRefPositions[index], Quaternion.identity);
			GUIDisplayRefs refs = guiDispObj.GetComponent<GUIDisplayRefs>();
			holder.displayRefs = refs;
			InitRefs(holder);
		}
	}

	void InitRefs(PlayerHolder holder)
	{
		holder.displayRefs.backgroundRend.sprite = playerBackgroundSprites[holder.playerSlotIndex];		//Set to the color for the holder's slot index
		holder.displayRefs.bulletRend.sprite = bulletStatusSprites[0];									//Default empty bullet sprite
		holder.displayRefs.chippyRend.sprite = chippyStatusSprites[0];									//Chippy mouth is empty by default
	}

	public void UpdateBulletRef(int bullets, PlayerHolder holder)
	{
		if(bullets >= bulletStatusSprites.Length)
		{
			Debug.LogWarning("Bullets index over the length of the array, not updating display.");
			return;
		}
		holder.displayRefs.bulletRend.sprite = bulletStatusSprites[bullets];
	}

	public void UpdateCandyRef(int candy, PlayerHolder holder)
	{
		if(candy >= chippyStatusSprites.Length)
		{
			Debug.LogWarning("Candy index over the length of the array, not updating display.");
			return;
		}
		holder.displayRefs.chippyRend.sprite = chippyStatusSprites[candy];
	}

	public void UpdateHolderScore(PlayerHolder holder)
	{
		holder.displayRefs.textInfo.text = holder.currentScore.ToString();
	}

	void StartChipmunkPlay()
	{
		Debug.Log ("Lets play chipmunks");
		CreateChipmunkPlayers();



		roundState = RoundState.Playing;
	}

	void CreateChipmunkPlayers()
	{
		for(int i=0;i<manager.maxPlayers;i++)
		{
			PlayerHolder holder = manager.players[i];

			holder.displayRefs.textInfo.text = holder.currentScore.ToString();
			AvatarSpawnPoint spawnPoint = avatarSpawnPoints[i];
			GameObject chipObj = (GameObject)Instantiate(chipmunkAvatarPrefab, spawnPoint.xForm.position, spawnPoint.xForm.rotation);
			Chipmunk chippy = chipObj.GetComponent<Chipmunk>();
			chippy.InitPlayeryThing(this, holder, chippyTintColors[holder.playerSlotIndex]);
			holder.avatar = chippy;
		}
	}

	public Transform GetPlayerSpawnPoint(PlayerHolder holder)
	{
		return avatarSpawnPoints[holder.playerSlotIndex].xForm;
	}

	void StartBirdiesPlay()
	{
		Debug.Log ("Lets play birdies");
		roundState = RoundState.Playing;
	}

	void InitBirdies()
	{
		Debug.Log ("TODO - Build Birdies init");
	}

	void InitMenu()
	{
		Debug.Log ("TODO - Build menu scene init");
	}

	#endregion

	#region Bullets

	void CreateBullet(Transform spawnPoint)
	{
		GameObject spearObj = (GameObject)Instantiate(spearPrefab, spawnPoint.position, spawnPoint.rotation);
		Bullet spearScript = spearObj.GetComponent<Bullet>();
		spearScript.InitBullet(this);
	}

	public void LostBullet()
	{
		CreateBullet(GetRandomObjectSpawn());
	}

	#endregion

	#region Candy

	void CreateCandyPiece(Transform spawnPoint)
	{
		GameObject candyObj = (GameObject)Instantiate(candyPiecePrefab, spawnPoint.position, spawnPoint.rotation);
		Candy candyScript = candyObj.GetComponent<Candy>();
		candyScript.InitCandy();
		AudioClip randomClip = candyArrivalClips[Random.Range(0, candyArrivalClips.Length - 1)];
		candyArrivalSource.PlayOneShot(randomClip);
	}

	public void LostCandyPiece()
	{
		Debug.Log ("Lost a piece of candy below the world");
		CreateCandyPiece(GetRandomObjectSpawn());
	}

	public Transform GetRandomObjectSpawn()
	{
		return objectSpawnPoints[Random.Range(0, objectSpawnPoints.Count - 1)];
	}

	#endregion

	#region Scoring

	//Called when a chunk of candy has been completely eaten
	public void CandyEaten(PlayerHolder holder, Candy candyEaten)
	{
		holder.AddScore(candyEaten.pointValue);
		UpdateHolderScore(holder);
		scoreChanged = true;
		StartCoroutine(DelayCandyCreation(candyRespawnDelay));
	}

	IEnumerator DelayCandyCreation(float delay)
	{
		yield return new WaitForSeconds(delay);
		CreateCandyPiece(GetRandomObjectSpawn());
	}

	//An example method not used anywhere
	public void MAMAWeJustKilledAMan(float amount)
	{
//		manager.AddScore(amount);
	}

	void CheckWinConditions()
	{
		foreach(PlayerHolder holder in manager.players)
		{
			if(holder.currentScore >= cheekyMaxScore)
			{
				//TODO - Probably should check for ties but its unlikely and not worth the time right now
				Debug.Log("We have a winner! Player index: " + holder.playerSlotIndex);
				RoundFinished();
				return;
			}
		}
	}

	public AudioSource finishedSource = null;

	public void RoundFinished()
	{
		if(roundState != RoundState.Playing)
			return;

		finishedSource.Play();

		roundState = RoundState.Finished;
		foreach(PlayerHolder holder in manager.players)
		{
			holder.RoundFinished();
		}
	}

	#endregion

	#region Updates

	void Update()
	{
//		if(levelType == LevelType.Menu || levelType == LevelType.Dragons || levelType == LevelType.Unicorns || levelType == LevelType.ThatOtherThing)
//			return;

		switch((int)roundState)
		{
			case (int)RoundState.Waiting:
			{
				manager.WatchForInputs();
				if(SlotsFilled())
					StartGameType(levelType);
				break;
			}
			case (int)RoundState.Playing:
			{
				if(playActive)
				{
					if(scoreChanged)
						CheckWinConditions();
				}
				break;
			}
			case (int)RoundState.Finished:
			{
				
				break;
			}
			case (int)RoundState.Loading:
			{
				
				break;
			}
			case (int)RoundState.ERROR:
			{
				
				break;
			}
		}

		if ( Input.GetKeyDown(KeyCode.Escape) )
		{
			Application.LoadLevel("MainMenu");
		}

	}

	#endregion
}
