﻿using UnityEngine;
using System.Collections;

public class MenuSelectButton : MonoBehaviour 
{

	public bool loadLevel = true;
	public string gameLevelLoadString;
	public Vector3 moveToMenuPosition;
	private Vector3 defaultScale;

	// Use this for initialization
	void Start () 
	{
		renderer.material.color = new Color(0.2f, 0.2f, 0.2f);
		defaultScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnMouseOver()
	{
		if(gameLevelLoadString.Length > 0 || !loadLevel)
		{
			renderer.material.color = Color.white;
			float rotZ = -5 + Mathf.PingPong(Time.time*40, 10);
			transform.rotation = Quaternion.Euler(new Vector3(0,180,rotZ));
			transform.localScale = defaultScale*1.2f;
		}
	}

	void OnMouseExit()
	{
		transform.rotation = Quaternion.Euler(new Vector3(0,180,0));
		renderer.material.color = new Color(0.2f, 0.2f, 0.2f);
		transform.localScale = defaultScale;
	}

	void OnMouseUpAsButton()
	{
		if(loadLevel)
		{
			Application.LoadLevel(gameLevelLoadString);
		}
		else
		{
			iTween.MoveTo(Camera.main.gameObject, iTween.Hash ("position", moveToMenuPosition, "time", 0.5f, "easetype", iTween.EaseType.easeInBack));
		}
	}
}
