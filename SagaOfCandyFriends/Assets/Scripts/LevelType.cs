﻿using UnityEngine;
using System.Collections;

public enum LevelType
{
	Menu,
	Chipmunks,
	Birdies,
	Unicorns,
	Dragons,
	ThatOtherThing
}
