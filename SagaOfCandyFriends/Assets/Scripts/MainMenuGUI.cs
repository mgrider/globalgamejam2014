﻿using UnityEngine;
using System.Collections;

public class MainMenuGUI : GUIComponent
{
	#region Variables

	public string[] levelNames = new string[1];
	public Texture[] levelTitleBar = new Texture[1];

	#endregion

	#region Levels

	void LoadGameLevel(string levelName)
	{
		Debug.Log("Trying to load game level");
//		if(levelManager.roundState != RoundState.Choosing)
//			return;
		Debug.Log("Loading new level");
		levelManager.roundState = RoundState.Loading;
		Application.LoadLevel(levelName);
	}

	#endregion

	#region GUI
	
	void OnGUI()
	{
		switch((int)levelManager.roundState)
		{
			case (int)RoundState.Waiting:
			{
				DisplayWaiting();
				break;
			}
//			case (int)RoundState.Choosing:
//			{
//				DisplayChoosing();
//				break;
//			}
			case (int)RoundState.Loading:
			{
				DisplayLoading();
				break;
			}
			case (int)RoundState.ERROR:
			{
				DisplayERROR();
				break;
			}
		}

		//always show level select
		//DisplayChoosing();
	}
	
	void DisplayWaiting()
	{
		if(gSkin != null)
			GUI.skin = gSkin;

		float centerX = (Screen.width * 0.5f);
		float centerY = (Screen.height * 0.5f);
		GUI.Box (new Rect(centerX - 60f,0f,120f,30f), "State: Waiting");

		for(int i=0;i<levelManager.manager.players.Length;i++)
		{
			string disp = "Filled";
			if(!levelManager.manager.players[i].playerSlotFilled)
			{
				disp = "Player " + (i + 1) + " press your action button.";
			}
			
			GUI.Box (new Rect(playerSlotPositions[i].x, playerSlotPositions[i].y, scoreBoxSize.x, scoreBoxSize.y), "");
			GUI.Label(new Rect(playerSlotPositions[i].x, playerSlotPositions[i].y, scoreBoxSize.x, scoreBoxSize.y), disp, "ScoreBox");
		}
		if(SlotsFilled())
			levelManager.roundState = RoundState.Choosing;

	}

	bool SlotsFilled()
	{
		return levelManager.manager.SlotsFilled();
	}

	void DisplayChoosing()
	{
		float centerX = (Screen.width * 0.5f);
		float centerY = (Screen.height * 0.5f);
		GUI.Box (new Rect(centerX - 60f,0f,120f,30f), "State: Waiting");
		
//		for(int i=0;i<levelNames.Length;i++)
//		{
//			if(GUI.Button(new Rect(centerX - 100f, centerY - 120f + (i * 30f), 200f, 30f), levelTitleBar[i], "label");//"Level: " + levelNames[i]))
//			{
//				LoadGameLevel(levelNames[i]);
//			}
//		}

		GUILayout.BeginArea(new Rect(Screen.width/2-300, Screen.height/5, 400,Screen.height/5*3));

		GUILayout.BeginVertical();
		for(int i=0;i<levelNames.Length;i++)
		{
			if(GUILayout.Button(levelTitleBar[i], "label"))
			{
				LoadGameLevel(levelNames[i]);
			}
		}
		GUILayout.EndVertical();

		GUILayout.EndArea();



	}
	void DisplayLoading()
	{
		GUI.Box (new Rect((Screen.width * 0.5f) - 60f,0f,120f,30f), "State: Loading");
	}
	void DisplayERROR()
	{
		GUI.Box (new Rect((Screen.width * 0.5f) - 60f,0f,120f,30f), "State: ERROR");
	}
	
	#endregion
}