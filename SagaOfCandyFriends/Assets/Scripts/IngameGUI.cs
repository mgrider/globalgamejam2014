﻿using UnityEngine;
using System.Collections;

public class IngameGUI : GUIComponent {

	#region Variables




	#endregion

	#region Levels

	void ReloadLevel()
	{
		Application.LoadLevel(Application.loadedLevelName);
	}

	void LoadMainMenu()
	{
		Application.LoadLevel("MainMenu");
	}

	#endregion

	#region GUI

	void OnGUI()
	{
		switch((int)levelManager.roundState)
		{
			case (int)RoundState.Waiting:
			{
				DisplayWaiting();
				break;
			}
			case (int)RoundState.Playing:
			{
				DisplayPlaying();
				break;
			}
			case (int)RoundState.Finished:
			{
				DisplayFinished();
				break;
			}
			case (int)RoundState.Loading:
			{
				DisplayLoading();
				break;
			}
			case (int)RoundState.ERROR:
			{
				DisplayERROR();
				break;
			}
		}
	}

	void DisplayWaiting()
	{
		if(gSkin != null)
			GUI.skin = gSkin;

		GUI.Box (new Rect((Screen.width * 0.5f) - 60f,0f,120f,30f), "State: Waiting");

		for(int i=0;i<levelManager.manager.players.Length;i++)
		{
			string disp = "Filled";
			if(!levelManager.manager.players[i].playerSlotFilled)
			{
				disp = "Player " + (i + 1) + " press your action button.";
			}

			GUI.Box (new Rect(playerSlotPositions[i].x, playerSlotPositions[i].y, scoreBoxSize.x, scoreBoxSize.y), "");
			GUI.Label(new Rect(playerSlotPositions[i].x, playerSlotPositions[i].y, scoreBoxSize.x, scoreBoxSize.y), disp, "ScoreBox");
		}
	}
	void DisplayPlaying()
	{
		if(gSkin != null)
			GUI.skin = gSkin;

		GUI.Box (new Rect((Screen.width * 0.5f) - 60f,0f,120f,30f), "State: Playing");

		for(int i=0;i<levelManager.manager.players.Length;i++)
		{
//			PlayerHolder holder = levelManager.manager.players[i];
//			float angle = Mathf.Atan2(-holder.lastSet.yIn, holder.lastSet.xIn) * Mathf.Rad2Deg;
//			float angle = Mathf.Atan2(holder.lastSet.xIn, holder.lastSet.yIn) * Mathf.Rad2Deg;
//			GUI.Box (new Rect(playerSlotPositions[i].x, playerSlotPositions[i].y, scoreBoxSize.x, scoreBoxSize.y), "");
//			GUI.Label(new Rect(playerSlotPositions[i].x, playerSlotPositions[i].y, scoreBoxSize.x, scoreBoxSize.y), "Score: " + angle + "\n" + "X: " + holder.lastSet.xIn.ToString("f1") + " | Y: " + holder.lastSet.yIn.ToString("f1"), "ScoreBox");
		}

		if(GUI.Button(new Rect((Screen.width * 0.5f) - 60f,Screen.height - 30,120f,30f), "Shortcut Finish"))
		{
			levelManager.RoundFinished();
		}
	}
	void DisplayFinished()
	{
		if(gSkin != null)
			GUI.skin = gSkin;

		float centerX = (Screen.width * 0.5f);
		float centerY = (Screen.height * 0.5f);

		GUI.Box (new Rect(centerX - 60f,0f,120f,30f), "State: Finished");

		GUI.Box (new Rect(centerX - 100, centerY - 40f, 200f, 80f), "");
		if(GUI.Button(new Rect(centerX - 60f, centerY - 30f, 120f, 30f), "Replay"))
		{
			ReloadLevel();
		}
		if(GUI.Button(new Rect(centerX - 60f, centerY, 120f, 30f), "Main Menu"))
		{
			LoadMainMenu();
		}
	}
	void DisplayLoading()
	{
		if(gSkin != null)
			GUI.skin = gSkin;

		GUI.Box (new Rect((Screen.width * 0.5f) - 60f,0f,120f,30f), "State: Loading");
	}
	void DisplayERROR()
	{
		if(gSkin != null)
			GUI.skin = gSkin;

		GUI.Box (new Rect((Screen.width * 0.5f) - 60f,0f,120f,30f), "State: ERROR");
	}

	#endregion
}
