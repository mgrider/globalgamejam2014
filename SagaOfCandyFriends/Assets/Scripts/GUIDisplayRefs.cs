﻿using UnityEngine;
using System.Collections;

public class GUIDisplayRefs : MonoBehaviour {

	public SpriteRenderer backgroundRend = null;
	public SpriteRenderer chippyRend = null;
	public SpriteRenderer bulletRend = null;
	public Renderer scoreRend = null;
	public TextMesh textInfo = null;
	public Renderer textBackRend = null;
}