﻿using UnityEngine;
using System.Collections;

public class Candy : MonoBehaviour {

	#region Variables

	public Transform xForm = null;
	public Renderer rend = null;
	public Rigidbody rigid = null;
	public Collider physicalCol = null;
	public Collider trigCol = null;

	public bool isActive = false;				//Activation flag after creation, changed in Init
	public bool canBePickedUp = false;			//Set true when players can pick it up
	public bool isHeld = false;					//Set true when a player has picked up the candy
	public bool isEaten = false;

	public Chipmunk candyHolder = null;			//Reference to the chipmunk and player that is currently holding the candy

	public float maxCapacity = 100;				//Max capacity for the points used in tracking eating progress
	public float currentCapacity = 100;			//Current capacity, this goes down as a chipmunk eats it, resets when dropped to max

	public float dropSpeed = 20f;

	public float pointValue = 20f;

	public float droppedStartTime = 0f;
	public float droppedDelay = 3f;

	public AudioSource wallHitSource = null;
	public AudioClip[] wallHitClips = new AudioClip[0];

	#endregion

	#region Init

	void Awake()
	{
		if(xForm == null)
			xForm = transform;

		if(rend == null)
			rend = renderer;

		if(rigid == null)
			rigid = rigidbody;
	}

	public void InitCandy()
	{
		isActive = true;
		PickMeUP();
	}

	#endregion

	#region Candy Events

	public void CandyPickedUp(Chipmunk newHolder)
	{
		candyHolder = newHolder;
		isHeld = true;
		DisableColliders();
		HideCandy();
	}

	void PickMeUP()
	{
		canBePickedUp = true;
	}

	void DisableColliders()
	{
		physicalCol.enabled = false;
		trigCol.enabled = false;
		rigid.isKinematic = true;
	}

	void EnableColliders()
	{
		physicalCol.enabled = true;
		trigCol.enabled = true;
		rigid.isKinematic = false;
	}

	public void ResetCandyCapacity()
	{
		currentCapacity = maxCapacity;
	}

	public void CandyDropped(Vector3 pos)
	{
		ShowCandy();
		EnableColliders();
		xForm.position = pos;
		xForm.parent = null;
		PushRandomDirection();
		candyHolder = null;
		canBePickedUp = false;
		droppedStartTime = Time.time;
		isHeld = false;
		currentCapacity = maxCapacity;
	}

	void PushRandomDirection()
	{
		float yRot = Random.Range(0f, 359f);
		xForm.rotation = Quaternion.Euler(new Vector3(0f, yRot, 0f));
		rigid.velocity = xForm.forward * dropSpeed;
	}

	void ShowCandy()
	{
		rend.enabled = true;
	}

	void HideCandy()
	{
		rend.enabled = false;
	}

	#endregion

	#region Eating

	public void CandyCapacityEaten(float amount)
	{
		if(amount <= 0)
		{
			Debug.LogWarning("Capacity eaten <= 0, returning");
			return;
		}

		currentCapacity -= amount;

		if(currentCapacity <= 0)
		{
			CandyCompletelyEaten();
		}
	}

	private void CandyCompletelyEaten()
	{
		if(isEaten)
			return;

		isEaten = true;
	}

	public float GetEatingPercent()
	{
		float perc = 1f;

		perc = Mathf.Clamp01(currentCapacity / maxCapacity);

		return perc;
	}

	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.layer == LayerMask.NameToLayer("Wall"))
		{
			Debug.Log ("Candy has hit the wall.");
			AudioClip randomClip = wallHitClips[Random.Range(0, wallHitClips.Length - 1)];
			wallHitSource.PlayOneShot(randomClip);
		}
	}

	#endregion

	#region Cleanup

	public void CleanUpCandy()
	{
		Destroy(gameObject);
	}

	#endregion

	#region Update

	void FixedUpdate()
	{
		if(!isHeld && !canBePickedUp && isActive)
		{
			if(Time.time > droppedStartTime + droppedDelay)
			{
				PickMeUP();
			}
		}
	}

	#endregion
}