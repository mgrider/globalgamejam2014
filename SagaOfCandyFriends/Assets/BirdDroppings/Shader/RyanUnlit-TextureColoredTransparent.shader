// Unlit shader. Simplest possible textured shader.
// - no lighting
// - no lightmap support
// - no per-material color
//
// UNITY shader: Unlit/Texture
//
//
// RYAN's shader addition of color

Shader "RyanUse/Unlit/Texture Colored Transparent" 
{
	Properties 
	{
		_Color ("Main Color", Color) = (1, 1, 1, 1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}

	SubShader 
	{
		Tags { "Queue"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha 
		LOD 100
		
		Pass 
		{
			Lighting Off
			SetTexture [_MainTex] 
			{ 
				constantColor [_Color]
				combine texture * constant
			} 
		}
	}
}
