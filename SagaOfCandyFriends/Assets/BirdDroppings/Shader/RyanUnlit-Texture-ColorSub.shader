Shader "RyanUse/Unlit/Texture-ColorSub" {
Properties {
 	_MainTex ("Base (RGB)", 2D) = "white" {}
 	_MaskCol ("Mask Color", Color)  = (1.0, 1.0, 1.0, 1.0)		
	_Color ("Main Color", Color) = (1, 1, 1, 1)
	_Threshold ("Threshold", Range(0,1)) = 0.75
}

SubShader {
		Tags { "Queue"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha 
    Pass {
		Lighting Off
		Cull Off
CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

sampler2D _MainTex;

struct v2f {
    float4  pos : SV_POSITION;
    float2  uv : TEXCOORD0;
};
float4 _MainTex_ST;

v2f vert (appdata_base v)
{
    v2f o;
    o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
    o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
    return o;
}

uniform float4 _MaskCol;
uniform float4 _Color;
uniform float _Threshold;

half4 frag (v2f i) : COLOR
{
				half4 col = tex2D(_MainTex, i.uv);
				
				if(distance( half4(col.rgb,0), half4(_MaskCol.rgb,0)) < _Threshold)
					col = half4(_Color.rgb,col.a);
				//return fixed4(1.0,0.0,0.0,1.0);
				return col;
}
ENDCG

    }
}
Fallback "VertexLit"
} 

