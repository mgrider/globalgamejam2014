// Unlit shader. Simplest possible textured shader.
// - no lighting
// - no lightmap support
// - no per-material color
//
// UNITY shader: Unlit/Texture

Shader "RyanUse/Unlit/Texture" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}

	SubShader 
	{
		Tags { "Queue"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha 
		LOD 100
		
		Pass 
		{
			Lighting Off
			Cull Off
			SetTexture [_MainTex] 
			{ 
				combine texture 
			} 
		}
	}
}
