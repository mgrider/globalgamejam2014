﻿using UnityEngine;
using System.Collections;

public class BirdFeet : MonoBehaviour 
{
	public BirdCandy birdCandy;
	public BirdController birdController;
	public Transform attachPoint;
	public bool isCloseToCandy;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(isCloseToCandy)
		{
			float distToCandy = Vector3.Distance(attachPoint.position, birdCandy.transform.position);
			if(distToCandy < 0.3f)
			{
				birdController.GrabCandy(birdCandy);
				birdCandy = null;
				isCloseToCandy = false;
			}
			//Debug.Log ("dist to candy: " + Vector3.Distance(attachPoint.position, birdCandy.transform.position) );
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		Debug.Log ("feet collider enter: " + other.name);
		birdCandy = other.GetComponent<BirdCandy>();
		if(birdCandy != null)
			isCloseToCandy = true;
	}

	void OnTriggerExit(Collider other)
	{
		Debug.Log ("feet collider exit: " + other.name);
		isCloseToCandy = false;
	}
}
