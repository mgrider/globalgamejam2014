using UnityEngine;
using System.Collections;

public class AnimTextureMaterial : MonoBehaviour 
{
    public float animationRate = 0.1f;
    public int gridX = 1;
    public int gridY = 1;
	public bool backwards = false;
	public bool playOnce = false;

    private float timer = 0;
    private float x = 0;
    private float y = 1;
    private float blockWidth;
    private float blockHeight;
	private int countFrames = 1;
	private int numFrames = 1;
	private bool doAnimate = true;
	
	private Vector2 startOffset;
	
    // Use this for initialization
    void Start()
    {
		startOffset = gameObject.renderer.sharedMaterial.mainTextureOffset;
		
        blockWidth = 1.0f / gridX;
        blockHeight = 1.0f / gridY;
		
		numFrames = gridX*gridY;
				
		//start at the first sprite
		y = 1 - blockHeight;
		x = 0;
		gameObject.renderer.sharedMaterial.mainTextureOffset = new Vector2(x, y);
	}
	
	void OnEnable()
	{		
		//Debug.Log (Time.time + " anim texturte onenable " + name + " count = " + countFrames);
		
		//start at the first sprite
		y = 1 - blockHeight;
		x = 0;	
		gameObject.renderer.sharedMaterial.mainTextureOffset = new Vector2(x, y);
		
		countFrames = 1;
		doAnimate = true;
	}
	
	void OnDestroy()
	{
		//Debug.Log("Restoring material " + gameObject.renderer.sharedMaterial.name);
		gameObject.renderer.sharedMaterial.mainTextureOffset = startOffset;
	}

    // Update is called once per frame
    void Update()
    {
		if(doAnimate)
		{
	        timer += Time.deltaTime;
	        if (timer > animationRate)
	        {
				countFrames++;
	            timer = 0;
				if(backwards)
				{
					x -= blockWidth;
					if( x < 0)
					{
						x = 1 - blockWidth;
						y+= blockHeight;
		                if (y >= 1)
		                    y = 0;
					}
				}
				
				else
				{
		            x += blockWidth;
		            if (x >= 1.0f)
		            {
		                x = 0;
						y -= blockHeight;
		            	if (y < 0)
		                	y = (gridY-1) * blockHeight;
		            }
				}
				
				gameObject.renderer.sharedMaterial.mainTextureOffset = new Vector2(x, y);
	        }
			
			if(countFrames >= numFrames)
			{
				countFrames = 1;
				if(playOnce)
					doAnimate = false;
			}
		}
    }
}
