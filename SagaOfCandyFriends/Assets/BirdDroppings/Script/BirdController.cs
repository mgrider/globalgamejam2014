﻿using UnityEngine;
using System.Collections;

public class BirdController : MonoBehaviour
{
	public MatchManager matchManager = null;

	public GameObject textureQuadFly;
	public GameObject textureQuadPick;
	public GameObject textureQuadPuff;
	public GameObject textureQuadDive;
	public GameObject featherPoofFX;
	public GameObject sparkleFX;
	private Transform textureQuads;
	public int playerNum;
	public bool diveSweetness = true;
	public float moveSpeed;
	public float turnRate = 1;
	
	//Vector3 myVelo;
	public float bonusSpeed = 0;
	public float speed = 0;
	float xIn, yIn;
	float angleX;
	float angleXtarget = 0;

	private BirdCandy birdCandy;
	public BirdFeet birdFeet;

	public int score = 0;
	private bool isHit = false;

	private bool isPlayerAlive = false;
	private float timerNoInput = 0;

	private bool isPlayerGettingInput = true;

	private bool gameIsFinished = false;

	public AudioClip[] hitSounds;
	public AudioClip[] grabSounds;
	public AudioClip sparkleSound;
	// Use this for initialization
	void Start () 
	{
		gameIsFinished = false;
		rigidbody.velocity = transform.forward * moveSpeed;
		textureQuads = textureQuadFly.transform.parent;
		angleX = rigidbody.velocity.x < 0 ? 180 : 0; //initial angle of motion
	}

	public void GameFinished()
	{
		gameIsFinished = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//sample idea of getting rid of non player birds
		//if(timerNoInput > 10)
		//	this.gameObject.SetActive(false);

		//myVelo = rigidbody.velocity;
		if(!isHit)
		{
			xIn = Input.GetAxis("P" + playerNum.ToString() + "_Horizontal") * moveSpeed;
			yIn = Input.GetAxis("P" + playerNum.ToString() + "_Vertical") * moveSpeed;
			Vector3 inputPointing = new Vector3(xIn, yIn, 0);

			if(inputPointing.sqrMagnitude > 0)
			{
				Debug.DrawLine(transform.position, transform.position + inputPointing, Color.red, Time.deltaTime, false);
				Debug.DrawLine(transform.position, transform.position + rigidbody.velocity, Color.yellow, Time.deltaTime, false);

				//the controller pointing direction
				angleXtarget = Mathf.Rad2Deg*Mathf.Atan2(-yIn, xIn);
				angleX = Mathf.LerpAngle(angleX, angleXtarget, Time.deltaTime * turnRate);

				timerNoInput = 0;
				isPlayerGettingInput = true;
			}
			else
			{
				timerNoInput += Time.deltaTime;
				if (isPlayerGettingInput && timerNoInput > 6) {
					angleX += 180;
					angleX %= 360;
					timerNoInput = 0;
				}
			}
		}
		else
		{			
			angleX = Mathf.LerpAngle(angleX, 90, Time.deltaTime);
		}
		transform.rotation = Quaternion.Euler(new Vector3(angleX, 90, 0));

		if(rigidbody.velocity.x < 0)
			textureQuads.localRotation = Quaternion.Euler(new Vector3(0,0,180));
		else			
			textureQuads.localRotation = Quaternion.Euler(new Vector3(0,0,0));

		if (diveSweetness) {
				//Here's the swoop accel, still with perfect direction changes
				float baseSpeed = 2.5f;
				float maxSpeed = 8.5f;
				bonusSpeed += -yIn * 0.02f;
				if (bonusSpeed < 0)
						bonusSpeed = 0;
				speed = baseSpeed + bonusSpeed;
				if (speed > maxSpeed) {
						speed = maxSpeed;
						bonusSpeed = maxSpeed - baseSpeed;
				}
				rigidbody.velocity = transform.forward * speed;
		} else {

				//Old-school fixed speed movement		
				rigidbody.velocity = transform.forward * moveSpeed;
		}

		transform.position = new Vector4(transform.position.x, transform.position.y, 0);



		//is it picking up a candy
		if(isHit)
		{
			ChangeSprite("puff");
		}
		else if(birdCandy == null)
		{
			if(birdFeet.isCloseToCandy)
			{
				//Debug.Log ("Close to candy");
				ChangeSprite("pick");
			}
			else
			{
				if(rigidbody.velocity.y < -0.75f*moveSpeed)
					ChangeSprite("dive");
				else
					ChangeSprite("fly");
			}
		}

		if(!gameIsFinished)
		{
			if(Input.GetButtonDown("P" + playerNum.ToString() + "_Button1"))
				DropCandy();

			if(Input.GetKeyDown(KeyCode.JoystickButton6))
				matchManager.EarlyMatchKill();
		}
		else
		{
			if(Input.GetButtonDown("P" + playerNum.ToString() + "_Button1"))
			{
				ResetLevel();
			}
			else if(Input.GetButtonDown("P" + playerNum.ToString() + "_Button2"))
			{
				ReturnToMainMenu();
			}
		}

//		if(Input.GetButtonDown("P" + playerNum.ToString() + "_Button1"))
//		{
//			iTween.ColorTo(gameObject, iTween.Hash("r", 0.0f, "g", 1.0f, "b", 0.0f, "time", 0.25f, "easetype", iTween.EaseType.easeInQuad));
//		}
//		if(Input.GetButtonUp ("P" + playerNum.ToString () + "_Button1"))
//		{
//			iTween.ColorTo(gameObject, iTween.Hash("r", 1.0f, "g", 1.0f, "b", 1.0f, "time", 0.25f, "delay", 0.0f, "easetype", iTween.EaseType.easeOutQuad));
//		}
//		
//		if(Input.GetButtonDown("P" + playerNum.ToString() + "_Button2"))
//		{
//			iTween.ColorTo(gameObject, iTween.Hash("r", 0.0f, "g", 0.0f, "b", 1.0f, "time", 0.25f, "easetype", iTween.EaseType.easeInQuad));
//		}
//		if(Input.GetButtonUp("P" + playerNum.ToString() + "_Button2"))
//		{
//			iTween.ColorTo(gameObject, iTween.Hash("r", 1.0f, "g", 1.0f, "b", 1.0f, "time", 0.25f, "delay", 0.0f, "easetype", iTween.EaseType.easeOutQuad));
//		}
	}

	void ReturnToMainMenu()
	{
		if(!gameIsFinished)
			return;

		gameIsFinished = false;
		matchManager.LoadMainMenu();
	}

	void ResetLevel()
	{
		if(!gameIsFinished)
			return;

		gameIsFinished = false;
		matchManager.ReloadLevel();
	}

//	void OnGUI()
//	{
//		Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
//		GUI.Box(new Rect(pos.x, Screen.height-pos.y, 60, 20), moveSpeedBonus.ToString("0.0"));
//	}

	public void GrabCandy(BirdCandy bc)
	{
		birdCandy = bc;
		birdCandy.transform.parent = birdFeet.attachPoint;
		birdCandy.transform.localPosition = Vector2.zero;
		birdCandy.rigidbody.isKinematic = true;
		birdCandy.transform.rotation = Quaternion.Euler(0, 0, birdCandy.transform.rotation.eulerAngles.z);
		birdCandy.gameObject.layer = LayerMask.NameToLayer("Feet");
		ChangeSprite("fly");
		birdFeet.collider.enabled = false; //disable feet, so doesn't grab more, and when released, doesn't regrab

		//reward a skill catch (catching a candy in the air) with a sparkle fx
		if (bc.birdWhoDropped != null) {
			if(diveSweetness)
				score += 1;
			GameObject sparkle = GameObject.Instantiate(sparkleFX, transform.position, Quaternion.identity) as GameObject;
			GameObject.Destroy(sparkle, 3);
			AudioSource.PlayClipAtPoint(sparkleSound, Camera.main.transform.position);
		}

		
		AudioSource.PlayClipAtPoint(grabSounds[Random.Range (0, grabSounds.Length)], Camera.main.transform.position);
	}

	void DropCandy()
	{
		if(birdCandy != null)
		{
			Debug.Log ("DROP CANDY!!!");
			birdCandy.WhoDroppedMe(this);
			birdCandy.transform.parent = null;
			birdCandy.transform.rotation = Quaternion.Euler(0, 0, birdCandy.transform.rotation.eulerAngles.z);
			birdCandy.rigidbody.isKinematic = false;	
			birdCandy.rigidbody.velocity = rigidbody.velocity;
			birdCandy.gameObject.layer = LayerMask.NameToLayer("Candy");
			birdCandy = null; //bird doesn't have the candy!
			Invoke ("ActivateFeet", 1f);

			AudioSource.PlayClipAtPoint(grabSounds[Random.Range (0, grabSounds.Length)], Camera.main.transform.position);
		}
	}

	void ActivateFeet()
	{
		birdFeet.collider.enabled = true;
	}

	void ChangeSprite(string sprite)
	{
		if(sprite == "fly")
		{
			textureQuadFly.SetActive(true);
			textureQuadPick.SetActive(false);
			textureQuadPuff.SetActive(false);
			textureQuadDive.SetActive(false);
		}
		else if(sprite == "pick")
		{
			textureQuadFly.SetActive(false);
			textureQuadPick.SetActive(true);
			textureQuadPuff.SetActive(false);
			textureQuadDive.SetActive(false);
		}
		else if(sprite == "puff")
		{
			textureQuadFly.SetActive(false);
			textureQuadPick.SetActive(false);
			textureQuadPuff.SetActive(true);
			textureQuadDive.SetActive(false);
		}
		else if(sprite == "dive")
		{
			textureQuadFly.SetActive(false);
			textureQuadPick.SetActive(false);
			textureQuadPuff.SetActive(false);
			textureQuadDive.SetActive(true);
		}
	}

	public void AddPoints(int points)
	{
		//do it here!
		score += points;

		//do other animation stuff or something
	}

	public void CandyHit()
	{
		GameObject poooooof = GameObject.Instantiate(featherPoofFX, birdFeet.attachPoint.transform.position, Quaternion.identity) as GameObject;  
		GameObject.Destroy(poooooof, 5);
		isHit = true;
		birdFeet.collider.enabled = false;
		
		AudioSource.PlayClipAtPoint(hitSounds[Random.Range (0, hitSounds.Length)], Camera.main.transform.position);
		DropCandy(); //force drop
		Invoke("UnHit", 1);
	}

	private void UnHit()
	{
		//stupid, so I can call it with Invoke.
		isHit = false;
		ActivateFeet();
	}

	//bird on bird collision, drop the candies
	//this isn't very fun
//	void OnCollisionEnter(Collision collision) 
//	{
//		if(!isHit)
//		{
//			if(collision.gameObject.tag == "Player")
//			{				
//				Debug.Log ("Bird " + gameObject.name + " on bird " + collision.gameObject.name);
//				CandyHit();
//			}
//		}
//	}
	
}
