﻿using UnityEngine;
using System.Collections;

public class StageAnimator : MonoBehaviour {

	public GameObject cloud1;
	public GameObject cloud2;
	public GameObject boat;
	public GameObject tentacle1;
	public GameObject tentacle2;
	public GameObject tentacle3;

	// Use this for initialization
	void Start () 
	{	
		iTween.RotateTo(tentacle1, iTween.Hash ("z", -15f, "time", 4f, "easetype", iTween.EaseType.easeInOutQuad, "looptype", iTween.LoopType.pingPong));
		iTween.RotateTo(tentacle2, iTween.Hash ("z", 15f, "time", 5f, "easetype", iTween.EaseType.easeInOutQuad, "looptype", iTween.LoopType.pingPong));
		iTween.RotateTo(tentacle3, iTween.Hash ("z", -15f, "time", 4.5f, "easetype", iTween.EaseType.easeInOutQuad, "looptype", iTween.LoopType.pingPong));
		iTween.RotateTo(boat, iTween.Hash ("z", -5f, "time", 7f, "easetype", iTween.EaseType.easeInOutQuad, "looptype", iTween.LoopType.pingPong));

		iTween.MoveTo(cloud1, iTween.Hash ("x", 0f, "time", 66f, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.loop));
		iTween.MoveTo(cloud2, iTween.Hash ("x", 40f, "time", 70f, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.loop));
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
}
