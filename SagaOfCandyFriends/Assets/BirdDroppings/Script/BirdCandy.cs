﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BirdCandy : MonoBehaviour {

	public BirdController birdWhoDropped;

	public List<BirdController> hitBirds;

	// Use this for initialization
	void Start () 
	{
		hitBirds = new List<BirdController>();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void WhoDroppedMe(BirdController bc)
	{
		Debug.Log ("WhoDroppedMe, " + bc.name);
		birdWhoDropped = bc;
		//clear the birds, and add to the ignore list
		
		hitBirds = new List<BirdController>();
		hitBirds.Add (bc);
	}

	public void HitABird(BirdController hitBird)
	{		
		if(birdWhoDropped != null)
		{
			bool validHit = true;
			string outp = "Hit a bird: " + hitBird.name + "\n";
			foreach(BirdController bc in hitBirds)
			{
				outp += "foreach loop on : " + bc.name + "\n";
				if(bc == hitBird)
				{
					validHit = false; //the hit bird is in the list, so not a point
				}
			}
			if(validHit)
			{
				birdWhoDropped.AddPoints(1);
				hitBirds.Add (hitBird); //add it to the ignore list
				hitBird.CandyHit(); //hit that bird, make him bleed!
			}
			Debug.Log (outp);
		}
	}

	void OnCollisionEnter(Collision collision) 
	{
		Debug.Log ("Collision in BirdCandy on " + collision.gameObject.name);
		if(collision.gameObject.tag == "Player")
		{
			Debug.Log("hit a player! ");
			BirdController hitBird = collision.gameObject.GetComponent<BirdController>();
			if(hitBird != null)
			{
				HitABird(hitBird);
			}
		}
		else if (collision.collider.tag == "Ground")
		{
			Debug.Log("hit the ground!");
			//need to know if it hits the ground, and then remove the WhoDroppedMe reference
			birdWhoDropped = null;
			//also clear the hit birds list
			hitBirds = new List<BirdController>();
		}
	}
}
