﻿using UnityEngine;
using System.Collections;

public class MatchManager : MonoBehaviour {

	public BirdController birdController1;
	public BirdController birdController2;
	public BirdController birdController3;
	public BirdController birdController4;

	public Renderer[] player1Mats;
	public Renderer[] player2Mats;
	public Renderer[] player3Mats;
	public Renderer[] player4Mats;
	
	public TextMesh flagScore1;
	public TextMesh flagScore2;
	public TextMesh flagScore3;
	public TextMesh flagScore4;

	public Color playerColor1 = Color.blue;
	public Color playerColor2 = Color.red;
//	public Color playerColor3 = Color.Lerp(Color.red, Color.blue, 0.5f);
	public Color playerColor3 = new Color(1,0,1);
	public Color playerColor4 = Color.green;
	
	// the score at which point the game is over
	private int GameOverScore = 5;

	// showing the game over screen
	public GameObject GameOverPane;
	public TextMesh GameOverOutcomeP1;
	public TextMesh GameOverOutcomeP2;
	public TextMesh GameOverOutcomeP3;
	public TextMesh GameOverOutcomeP4;

	// restarting the game after game over
	private float timerGameHasBeenOver = 0;
	private bool gameIsOver = false;
	
	// Use this for initialization
	void Start () 
	{
		foreach(Renderer rr in player1Mats)
			rr.material.color = playerColor1;
		foreach(Renderer rr in player2Mats)
			rr.material.color = playerColor2;
		foreach(Renderer rr in player3Mats)
			rr.material.color = playerColor3;
		foreach(Renderer rr in player4Mats)
			rr.material.color = playerColor4;

		// hide game over
		GameOverPane.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if ( Input.GetKeyDown(KeyCode.Escape) )
		{
			StartGameOver ();
			LoadMainMenu();
		}
		if (gameIsOver) {
			timerGameHasBeenOver += Time.deltaTime;
			if (timerGameHasBeenOver > 0.1f )
			{
				StartGameOver();
			}
		}
		int score1 = birdController1.score;
		int score2 = birdController2.score;
		int score3 = birdController3.score;
		int score4 = birdController4.score;
		flagScore1.text = score1.ToString ();
		flagScore2.text = score2.ToString ();
		flagScore3.text = score3.ToString ();
		flagScore4.text = score4.ToString ();
		// check for game over
		bool showGameOver = false;
		if (score1 >= GameOverScore) {
			showGameOver = true;
			GameOverOutcomeP1.text = "Not a loser";
			GameOverOutcomeP1.color = Color.green;
			GameOverOutcomeP2.text = "Loser";
			GameOverOutcomeP2.color = Color.red;
			GameOverOutcomeP3.text = "Loser";
			GameOverOutcomeP3.color = Color.red;
			GameOverOutcomeP4.text = "Loser";
			GameOverOutcomeP4.color = Color.red;
		}
		else if (score2 >= GameOverScore) {
			showGameOver = true;
			GameOverOutcomeP1.text = "Loser";
			GameOverOutcomeP1.color = Color.red;
			GameOverOutcomeP2.text = "Not a loser";
			GameOverOutcomeP2.color = Color.green;
			GameOverOutcomeP3.text = "Loser";
			GameOverOutcomeP3.color = Color.red;
			GameOverOutcomeP4.text = "Loser";
			GameOverOutcomeP4.color = Color.red;
		}
		else if (score3 >= GameOverScore) {
			showGameOver = true;
			GameOverOutcomeP1.text = "Loser";
			GameOverOutcomeP1.color = Color.red;
			GameOverOutcomeP2.text = "Loser";
			GameOverOutcomeP2.color = Color.red;
			GameOverOutcomeP3.text = "Not a loser";
			GameOverOutcomeP3.color = Color.green;
			GameOverOutcomeP4.text = "Loser";
			GameOverOutcomeP4.color = Color.red;
		}
		else if (score4 >= GameOverScore)
		{
			showGameOver = true;
			GameOverOutcomeP1.text = "Loser";
			GameOverOutcomeP1.color = Color.red;
			GameOverOutcomeP2.text = "Loser";
			GameOverOutcomeP2.color = Color.red;
			GameOverOutcomeP3.text = "Loser";
			GameOverOutcomeP3.color = Color.red;
			GameOverOutcomeP4.text = "Not a loser";
			GameOverOutcomeP4.color = Color.green;
		}
		if (showGameOver) {
			ShowGameOver ();
		}
	}

	public void EarlyMatchKill()
	{
		GameOverOutcomeP1.text = "Incomplete...";
		GameOverOutcomeP1.color = Color.blue;
		GameOverOutcomeP2.text = "Incomplete...";
		GameOverOutcomeP2.color = Color.blue;
		GameOverOutcomeP3.text = "Incomplete...";
		GameOverOutcomeP3.color = Color.blue;
		GameOverOutcomeP4.text = "Incomplete...";
		GameOverOutcomeP4.color = Color.blue;
		ShowGameOver();
	}

	void ShowGameOver () {
		Debug.Log ("Game Is Over.");
		// pause everything
		Time.timeScale = 0.01f;
		GameOverPane.SetActive (true);
		gameIsOver = true;
		birdController1.GameFinished();
		birdController2.GameFinished();
		birdController3.GameFinished();
		birdController4.GameFinished();
	}

	void StartGameOver ()
	{
		gameIsOver = false;
		timerGameHasBeenOver = 0;
		birdController1.score = 0;
		birdController2.score = 0;
		birdController3.score = 0;
		birdController4.score = 0;
		GameOverPane.SetActive (false);
		Time.timeScale = 1.0f;
		ReloadLevel ();
	}

	#region level loading

	public void ReloadLevel()
	{
		Time.timeScale = 1f;
		Application.LoadLevel(Application.loadedLevelName);
	}
	
	public void LoadMainMenu()
	{
		Time.timeScale = 1f;
		Application.LoadLevel("MainMenu");
	}

	#endregion

}
